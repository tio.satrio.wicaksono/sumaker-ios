//
//  BaseResponse.swift
//  Pengaduan KLHK
//
//  Created by Satrio Wicaksono on 16/04/2018.
//  Copyright © 2018 Docotel Group. All rights reserved.
//

import Foundation

struct BaseResponse<T: Codable>: Codable {
    var status: String
    var data: Array<T>
    var error: ErrorModel?
}

struct ErrorModel: Codable {
    var title: String
    var detail: String
    var code: String?
}


