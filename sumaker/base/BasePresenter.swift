//
//  BasePresenter.swift
//  iOS Base Project
//
//  Created by Lafran Pane on 1/12/18.
//  Copyright © 2018 Docotel Group. All rights reserved.
//

import Foundation

protocol PresenterCommonDelegate {
    

}

class BasePresenter<T>: PresenterCommonDelegate {
    var view: T!

    init(view: T!) {
        self.view = view!
    }
}
