//
//  DummyUtil.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 26/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class DummyUtil{
    
    static func initDummySuratHistoryModel() -> Array<SuratHistoryModel>{
        var list = Array<SuratHistoryModel>()
        let suratHistory1 = SuratHistoryModel()
        suratHistory1.nama = "Satrio Wicaksono"
        suratHistory1.kepada = "Tio"
        suratHistory1.image = "ProfilePlaceholder"
        suratHistory1.waktu = "2018-07-16 17:17:17"
        list.append(suratHistory1)
        list.append(suratHistory1)
        list.append(suratHistory1)
        return list
    }
    
    static func initDummySuratInfoModel() -> Array<SuratInfoModel>{
        var list = Array<SuratInfoModel>()
        let suratInfo1 = SuratInfoModel(tglKirim: "10 Agustus 2018, 16:52:43", tglBaca: "11 Agustus 2018, 16:52:43", dari: "TU SEKJEN (STAF STAF TU)", tujuan: "TU STAF AHLI (STAF STAF TU)", parafImage: "")
        let suratInfo2 = SuratInfoModel(tglKirim: "12 Agustus 2018, 16:52:43", tglBaca: "13 Agustus 2018, 16:52:43", dari: "TU SEKJEN (STAF STAF TU)", tujuan: "TU STAF AHLI (STAF STAF TU)", parafImage: "")
        list.append(suratInfo1)
        list.append(suratInfo2)
        return list
        
    }
}
