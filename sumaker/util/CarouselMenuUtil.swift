//
//  CarouselMenuUtil.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 30/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class CarouselMenuUtil{
    static func initDataModel() -> Array<CarouselMenuModel>{
        var list = Array<CarouselMenuModel>()
        list.append(CarouselMenuModel(image: UIImage(named: "InboxIcon")!, menu: "Surat Masuk"))
        list.append(CarouselMenuModel(image: UIImage(named: "DistributionExternalIcon")!, menu: "Distribusi Surat Masuk"))
        list.append(CarouselMenuModel(image: UIImage(named: "DistributionInternalIcon")!, menu: "Distribusi Surat Keluar"))
        list.append(CarouselMenuModel(image: UIImage(named: "SendIcon")!, menu: "Surat Terkirim"))
        list.append(CarouselMenuModel(image: UIImage(named: "ProfilePlaceholder")!, menu: "Profil"))
        list.append(CarouselMenuModel(image: UIImage(named: "CalendarIcon")!, menu: "Agenda"))
        return list
    }
}
