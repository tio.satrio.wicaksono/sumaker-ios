//
//  DownloadUtil.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 17/12/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class DownloadUtil{
    
    static var instance: DownloadUtil!
    var delegate: DownloadUtilDelegate?
    
    static func getInstance() -> DownloadUtil{
        if instance == nil {
            instance = DownloadUtil()
        }
        return instance
    }
    
    func download(url: String, delegates: DownloadUtilDelegate) {
        // Initiated delegate
        delegate = delegates
        
        //Create URL to the source file you want to download
        let fileURL = URL(string: url)
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:fileURL!)
        
        // Create destination URL
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        let destinationFileUrl = documentsUrl.appendingPathComponent(fileURL?.lastPathComponent ?? "")
        
        self.delegate?.taskDidBegin()
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                
                do {
                    
                    if FileManager.default.fileExists(atPath: destinationFileUrl.path, isDirectory: nil) {
                        try? FileManager.default.removeItem(at: destinationFileUrl)
                    }
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    self.delegate?.onDownloadSuccess(url: destinationFileUrl)
                } catch (let writeError) {
                    let errorMsg = "Error creating a file \(destinationFileUrl) : \(writeError)"
                    print(errorMsg)
                    self.delegate?.taskDidError(message: errorMsg)
                    self.delegate?.taskDidFinish()
                }
                
            } else {
                let errorMsg = "Error took place while downloading a file. Error description: \(error?.localizedDescription)"
                self.delegate?.taskDidError(message: errorMsg)
                print(errorMsg)
            }
            self.delegate?.taskDidFinish()
        }
        task.resume()
    }
}

protocol DownloadUtilDelegate: BaseDelegate {
    func onDownloadSuccess(url: URL)
}
