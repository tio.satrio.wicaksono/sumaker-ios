//
//  DBUtil.swift
//
//  Created by Satrio Wicaksono on 07/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import SugarRecord

class DBUtil: NSObject {
    static var dbUtil: DBUtil!
    var db: CoreDataDefaultStorage!
    var predicate: NSPredicate? = nil
    
    static func getInstance() -> DBUtil{
        if dbUtil == nil {
            dbUtil = DBUtil()
            dbUtil.initialize()
        }
        return dbUtil
    }
    
    func initialize(){
        let store = CoreDataStore.named("SUMAKER")
        let bundle = Bundle(for: self.classForCoder)
        let model = CoreDataObjectModel.merged([bundle])
        db = try! CoreDataDefaultStorage(store: store, model: model)
    }
    
    func saveUserIdentity(loginModel: LoginModel){
        deleteCurrentUser()
        var currentUser: CurrentUser!
        do {
            try db.operation({ (context, save) throws in
                currentUser = try context.new() as CurrentUser
                currentUser.token = loginModel.token
                currentUser.userId = loginModel.userId
                save()
            })
        }catch{
            print("Gagal simpan data current user")
        }
    }
    
    
    func getCurrentUser() -> CurrentUser?{
        var currentUser: CurrentUser? = nil
        do {
            try db.operation({ (context, save) throws in
                currentUser = try context.request(CurrentUser.self).fetch().first
            })
        }catch{
            return currentUser
        }
        return currentUser
    }
    
    func deleteCurrentUser(){
        do {
            try db.operation({ (context, save) throws in
                let currentUser = try context.request(CurrentUser.self).fetch()
                try context.remove(currentUser)
                save()
                print("Hapus data berhasil")
            })
        }catch{
            print("Gagal hapus data")
        }
    }
}

class TokenUtil {
    
    static func get() -> String? {
        guard let currentUser = DBUtil.getInstance().getCurrentUser() else {
            return nil
        }
        return currentUser.token
    }
    
}
