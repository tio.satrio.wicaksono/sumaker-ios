//
//  ValidatorUtil.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 23/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit
import FileBrowser

enum ValidatorType {
    case Number
    case Email
}

class ValidatorUtil{
    static var instance: ValidatorUtil!
    
    static func getInstance() -> ValidatorUtil {
        if instance == nil {
            instance = ValidatorUtil()
        }
        return instance
    }
    
    func validateText(text: String, type: ValidatorType?, name: String?) throws -> String {
        if text.isEmpty {
            if name != nil {
                throw ValidatorError.error(reason: "Inputan \(name!) tidak boleh kosong.")
            } else {
                throw ValidatorError.error(reason: "Inputan tidak boleh kosong.")
            }
        } else {
            switch type {
            case .Number?:
                return try validateNumber(text: text, name: name)
            case .Email?:
                return try validateEmail(text: text, name: name)
            default:
                break
            }
            return text
        }
    }
    
    func validatePickerView(text: String, name: String?) throws {
        if text.isEmpty {
            if name != nil {
                throw ValidatorError.error(reason: "Inputan \(name) tidak boleh kosong")
            } else {
                throw ValidatorError.error(reason: "Inputan tidak boleh kosong")
            }
        }
    }
    
    func validatePassword(pass: String, retryPass: String) throws -> String {
        if pass.isEmpty || retryPass.isEmpty {
            throw ValidatorError.error(reason: "Inputan password tidak boleh kosong")
        } else {
            if pass == retryPass {
                return pass
            } else {
                throw ValidatorError.error(reason: "Konfirmasi password tidak sama")
            }
        }
    }
    
    func validateNumber(text: String, name: String?) throws -> String {
        if !text.isValidNumber() {
            if name != nil {
                throw ValidatorError.error(reason: "Inputan \(name) tidak valid.")
            } else {
                throw ValidatorError.error(reason: "Inputan tidak valid.")
            }
        }
        return text
    }
    
    func validateFile(file: FBFile?) throws -> FBFile {
        if file == nil {
            throw ValidatorError.error(reason: "File surat tidak boleh kosong")
        }
        return file!
    }
    
    func validateEmail(text: String, name: String?) throws -> String {
        if !text.isValidEmail() {
            if name != nil {
                throw ValidatorError.error(reason: "Inputan \(name) tidak valid.")
            } else {
                throw ValidatorError.error(reason: "Inputan tidak valid.")
            }
        }
        return text
    }
}

enum ValidatorError: Error{
    case error(reason: String)
}


