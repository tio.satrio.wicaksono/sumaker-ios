//
//  FillPickerViewUtil.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 24/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class FillPickerViewUtil{
    
    static func getDataSuratDari() -> Array<PickerViewModel>{
        var list = Array<PickerViewModel>()
        list.append(PickerViewModel(id: 0, nama: "Semua"))
        list.append(PickerViewModel(id: 1, nama: "Setjen"))
        list.append(PickerViewModel(id: 2, nama: "Eselon 1"))
        list.append(PickerViewModel(id: 3, nama: "Kanwil"))
        list.append(PickerViewModel(id: 4, nama: "UPT"))
        return list
    }
    
    static func getDataKategoriSurat() -> Array<PickerViewModel>{
        var list = Array<PickerViewModel>()
        list.append(PickerViewModel(id: 0, nama: "Semua"))
        list.append(PickerViewModel(id: 1, nama: "Biasa"))
        list.append(PickerViewModel(id: 2, nama: "Penting"))
        list.append(PickerViewModel(id: 3, nama: "Segera"))
        list.append(PickerViewModel(id: 4, nama: "Sangat Segera"))
        list.append(PickerViewModel(id: 5, nama: "Rahasia"))
        return list
    }
    
    static func getDataSifatSurat() -> Array<PickerViewModel>{
        var list = Array<PickerViewModel>()
        list.append(PickerViewModel(id: 0, nama: "Biasa"))
        list.append(PickerViewModel(id: 1, nama: "Penting"))
        list.append(PickerViewModel(id: 2, nama: "Segera"))
        list.append(PickerViewModel(id: 3, nama: "Sangat Segera"))
        list.append(PickerViewModel(id: 4, nama: "Rahasia"))
        return list
    }
    
    static func getDataDitujukanKepada() -> Array<PickerViewModel>{
        var list = Array<PickerViewModel>()
        list.append(PickerViewModel(id: 0, nama: "Pusat Data dan Teknologi Informasi"))
        list.append(PickerViewModel(id: 1, nama: "Kepala Pusat Data dan Teknologi Informasi"))
        list.append(PickerViewModel(id: 2, nama: "Kepala Bagian Tata Usaha dan Umum"))
        return list
    }
}
