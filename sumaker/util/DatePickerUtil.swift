//
//  DatePickerUtil.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 20/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class DatePickerUtil{
    static var formatter = DateFormatter()
    
    static func formatDate(date: Date) -> String {
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter.string(from:date)
    }
    
    static func formatDateTime(date: Date) -> String {
        formatter.dateFormat = "YYYY-MM-dd HH:mm"
        return formatter.string(from:date)
    }
    
    static func dateFromString(string: String) -> Date {
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter.date(from: string)!
    }
}
