//
//  ApiProvider.swift
//
//
//  Created by Satrio Wicaksono on 16/04/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import Moya
import FileBrowser

enum ApiProvider {
    // MARK: List of API
    case login(username: String, password: String)
    case getUserProfile(userId: String, key: String)
    case getInboxTab(userId: String, key: String)
    case getSifatSurat(key: String)
    case getSuratMasuk(userId: String, originId: String, readStatus: String, sifatId: String,
                        processStatus: String, limitWaktuStatus: String, offset: Int,
                        numRow: Int, keyword: String, key: String)
    case getSuratKeluar(userId: String, originId: String, readStatus: String, sifatId: String,
        processStatus: String, limitWaktuStatus: String, offset: Int,
        numRow: Int, keyword: String, key: String)
    case getSuratInfoHistory(mailId: String, key: String)
    case getDitujukanKepada(userId: String, key: String)
    case readStatusSurat(mailId: String, status: String, key: String)
    case getSifatSuratDistribusi(userId: String)
    case sendDistribusiSuratMasuk(userIdKoseptor: String, idPegawaiTujuans: String, userFile: FBFile, fileLampiran: FBFile?,
                                    noSurat: String, tglSurat: String, sifatSurat: Int,
                                    perihal: String, isiSurat: String, dariSurat: String, kepadaSurat: String)
    case sendDistribusiSuratKeluar(userIdKoseptor: String, userIdTtd: String, idPegawaiTujuans: String, userFile: FBFile,                                                                                               fileLampiran: FBFile?, noSurat: String, tglSurat: String, sifatSurat: Int,
        perihal: String, isiSurat: String, dariSurat: String, kepadaSurat: String)
    case getTotalUnread(userId: String)
    case getInboxRead(mailId: String, key: String)
    case getOutboxRead(mailId: String, key: String)
    case getPenandatanganan(userId: String)
    case getAgenda(userId: String, bulan: String, tahun: String)
}

extension ApiProvider: TargetType{
    
    var sampleData: Data {
        return Data()
    }
    
    var baseURL: URL {
        guard var url = URL(string: Api.BASE_URL_API) else { fatalError("Server Bermasalah") }
        switch self {
        case .login:
            url = URL(string: Api.BASE_URL_API)!
            return url
        case .getUserProfile(_, let key):
            url = URL(string: Api.BASE_URL_API + "/user-details/?key=\(key)")!
            return url
        case .getInboxTab(_, let key):
            url = URL(string: Api.BASE_URL_API + "/inbox-tabs/?key=\(key)")!
            return url
        case .getSifatSurat(let key):
            url = URL(string: Api.BASE_URL_API + "/mail-sifat/?key=\(key)")!
            return url
        case .getSuratMasuk(_, _, _, _, _, _, _, _, _, let key):
            url = URL(string: Api.BASE_URL_API + "/inbox-mails/?key=\(key)")!
            return url
        case .getSuratKeluar(_, _, _, _, _, _, _, _, _, let key):
            url = URL(string: Api.BASE_URL_API + "/outbox-mails/?key=\(key)")!
            return url
        case .getSuratInfoHistory(_, let key):
            url = URL(string: Api.BASE_URL_API + "/inbox-history-distribusi/?key=\(key)")!
            return url
        case .getDitujukanKepada(_, let key):
            url = URL(string: Api.BASE_URL_API + "/mail-tujuan-kirim/?key=\(key)")!
            return url
        case .readStatusSurat(_, _, let key):
            url = URL(string: Api.BASE_URL_API + "/inbox-change-read-status/?key=\(key)")!
            return url
        case .getSifatSuratDistribusi:
            url = URL(string: Api.BASE_URL_ESAKI + "/get_sifat_surat")!
            return url
        case .sendDistribusiSuratMasuk:
            url = URL(string: Api.BASE_URL_ESAKI + "/distribusi_surat_masuk")!
            return url
        case .sendDistribusiSuratKeluar:
            url = URL(string: Api.BASE_URL_ESAKI + "/distribusi_surat_keluar")!
            return url
        case .getTotalUnread:
            url = URL(string: Api.BASE_URL_ESAKI + "/jumlah_inbox_unread")!
            return url
        case .getInboxRead(_, let key):
            url = URL(string: Api.BASE_URL_API + "/inbox-read/?key=\(key)")!
            return url
        case .getOutboxRead(_, let key):
            url = URL(string: Api.BASE_URL_API + "/outbox-read/?key=\(key)")!
            return url
        case .getPenandatanganan:
            url = URL(string: Api.BASE_URL_ESAKI + "/get_penandatangan")!
            return url
        case .getAgenda:
            url = URL(string: Api.BASE_URL_ESAKI + "/agenda")!
            return url
        default:
            return url
        }
    }

    var path: String {
        switch self {
        case .login:
            return "/login/"
        case .getUserProfile:
            return ""
        case .getInboxTab:
            return ""
        case .getSifatSurat:
            return ""
        case .getSuratMasuk:
            return ""
        case .getSuratKeluar:
            return ""
        case .getSuratInfoHistory:
            return ""
        case .getDitujukanKepada:
            return ""
        case .readStatusSurat:
            return ""
        case .getSifatSuratDistribusi:
            return ""
        case .sendDistribusiSuratMasuk:
            return ""
        case .sendDistribusiSuratKeluar:
            return ""
        case .getTotalUnread:
            return ""
        case .getInboxRead:
            return ""
        case .getOutboxRead:
            return ""
        case .getPenandatanganan:
            return ""
        case .getAgenda:
            return ""
        }
    }

    var method: Moya.Method {
        switch self {
        case .login,
             .getUserProfile,
             .getInboxTab,
             .getSifatSurat,
             .getSuratMasuk,
             .getSuratKeluar,
             .getSuratInfoHistory,
             .getDitujukanKepada,
             .readStatusSurat,
             .getSifatSuratDistribusi,
             .sendDistribusiSuratMasuk,
             .sendDistribusiSuratKeluar,
             .getTotalUnread,
             .getInboxRead,
             .getOutboxRead,
             .getPenandatanganan,
             .getAgenda:
            return .post
        }
    }

    var parameters: [String: Any] {
        switch self {
        case .login(let username,let password):
            var parameters = [String: Any]()
            parameters["username"] = username
            parameters["password"] = password
            return parameters
        
        case .getUserProfile(let userId, _),
             .getInboxTab(let userId, _):
            var parameters = [String: Any]()
            parameters["user_id"] = userId
            return parameters
            
        case .getSuratMasuk(let userId,let originId,let readStatus,let sifatId, let processStatus,
                                let limitWaktuStatus,let offset,let numRow,let keyword, _):
            var parameters = [String: Any]()
            parameters["user_id"] = userId
            parameters["origin_id"] = originId
            parameters["read_status"] = readStatus
            parameters["sifat_id"] = sifatId
            parameters["proses_status"] = processStatus
            parameters["limit_waktu_status"] = limitWaktuStatus
            parameters["offset"] = offset
            parameters["num_row"] = numRow
            parameters["keyword"] = keyword
            return parameters
            
        case .getSuratKeluar(let userId,let originId,let readStatus,let sifatId, let processStatus,
                            let limitWaktuStatus,let offset,let numRow,let keyword, _):
            var parameters = [String: Any]()
            parameters["user_id"] = userId
            parameters["origin_id"] = originId
            parameters["read_status"] = readStatus
            parameters["sifat_id"] = sifatId
            parameters["proses_status"] = processStatus
            parameters["limit_waktu_status"] = limitWaktuStatus
            parameters["offset"] = offset
            parameters["num_row"] = numRow
            parameters["keyword"] = keyword
            return parameters
            
        case .getSuratInfoHistory(let mailId, _):
            var parameters = [String: Any]()
            parameters["mail_id"] = mailId
            return parameters
        case .getDitujukanKepada(let userId, _):
            var parameters = [String: Any]()
            parameters["user_id"] = userId
            return parameters
        case .readStatusSurat(let mailId, let status, _):
            var parameters = [String: Any]()
            parameters["mail_id"] = mailId
            parameters["status"] = status
            return parameters
        case .getSifatSuratDistribusi(let userId):
            var parameters = [String: Any]()
            parameters["user_id"] = userId
            return parameters
        case .sendDistribusiSuratMasuk(let userIdKoseptor, let idPegawaiTujuans, _,
                                       _, let noSurat, let tglSurat, let sifatSurat, let perihal,
                                       let isiSurat, let dariSurat, let kepadaSurat):
            var parameters = [String: Any]()
            parameters["user_id_konseptor"] = userIdKoseptor
            parameters["id_pegawai_tujuans"] = idPegawaiTujuans
            parameters["no_surat"] = noSurat
            parameters["tgl_surat"] = tglSurat
            parameters["sifat_surat"] = sifatSurat
            parameters["perihal"] = perihal
            parameters["isi_surat"] = isiSurat
            parameters["dari_surat"] = dariSurat
            parameters["kepada_surat"] = kepadaSurat
            return parameters
        case .sendDistribusiSuratKeluar(let userIdKoseptor, let userIdTtd, let idPegawaiTujuans, _,
                                       _, let noSurat, let tglSurat, let sifatSurat, let perihal,
                                       let isiSurat, let dariSurat, let kepadaSurat):
            var parameters = [String: Any]()
            parameters["user_id_konseptor"] = userIdKoseptor
            parameters["user_id_ttd"] = userIdTtd
            parameters["id_pegawai_tujuans"] = idPegawaiTujuans
            parameters["no_surat"] = noSurat
            parameters["tgl_surat"] = tglSurat
            parameters["sifat_surat"] = sifatSurat
            parameters["perihal"] = perihal
            parameters["isi_surat"] = isiSurat
            parameters["dari_surat"] = dariSurat
            parameters["kepada_surat"] = kepadaSurat
            return parameters
        case .getTotalUnread(let userId):
            var parameters = [String: Any]()
            parameters["user_id"] = userId
            return parameters
        case .getInboxRead(let mailId, _):
            var parameters = [String: Any]()
            parameters["mail_id"] = mailId
            return parameters
        case .getOutboxRead(let mailId, _):
            var parameters = [String: Any]()
            parameters["mail_id"] = mailId
            return parameters
        case .getPenandatanganan(let userId):
            var parameters = [String: Any]()
            parameters["user_id"] = userId
            return parameters
        case .getAgenda(let userId,let bulan,let tahun):
            var parameters = [String: Any]()
            parameters["user_id"] = userId
            parameters["bulan"] = bulan
            parameters["tahun"] = tahun
            return parameters
        default:
            return [:]
        }
    }

    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var task: Task {
        switch self {
        case .login,
             .getUserProfile,
             .getInboxTab,
             .getSuratMasuk,
             .getSuratKeluar,
             .getSuratInfoHistory,
             .getDitujukanKepada,
             .readStatusSurat,
             .getSifatSuratDistribusi,
             .getTotalUnread,
             .getInboxRead,
             .getOutboxRead,
             .getPenandatanganan,
             .getAgenda:
            return .requestParameters(parameters: parameters, encoding: parameterEncoding)
        case .getSifatSurat:
            return .requestPlain
        case .sendDistribusiSuratMasuk(let userIdKoseptor, let idPegawaiTujuans, let userFile, let fileLampiran,
                                       let noSurat, let tglSurat, let sifatSurat, let perihal,
                                       let isiSurat, let dariSurat, let kepadaSurat):
            var multipartData = [MultipartFormData]()
            var sifatSuratId = sifatSurat.description
            let userIdKonseptorMultipart = MultipartFormData(provider: .data(userIdKoseptor.data(using: .utf8)!), name: "user_id_konseptor")
            let idPegawaiTujuansMultipart = MultipartFormData(provider: .data(idPegawaiTujuans.data(using: .utf8)!), name: "id_pegawai_tujuans")
            let noSuratMultipart = MultipartFormData(provider: .data(noSurat.data(using: .utf8)!), name: "no_surat")
            let tglSuratMultipart = MultipartFormData(provider: .data(tglSurat.data(using: .utf8)!), name: "tgl_surat")
            let sifatSuratMultipart = MultipartFormData(provider: .data(sifatSuratId.data(using: .utf8)!), name: "sifat_surat")
            let perihalMultipart = MultipartFormData(provider: .data(perihal.data(using: .utf8)!), name: "perihal")
            let isiSuratMultipart = MultipartFormData(provider: .data(isiSurat.data(using: .utf8)!), name: "isi_surat")
            let dariSuratMultipart = MultipartFormData(provider: .data(dariSurat.data(using: .utf8)!), name: "dari_surat")
            let kepadaSuratMultipart = MultipartFormData(provider: .data(kepadaSurat.data(using: .utf8)!), name: "kepada_surat")
            
            let userFileData = try! Data(contentsOf: userFile.filePath)
            let userFileMultipart = MultipartFormData(provider: .data(userFileData), name: "userfile", fileName: userFile.displayName, mimeType: "*.*")
    
            multipartData.append(userFileMultipart)
            multipartData.append(userIdKonseptorMultipart)
            multipartData.append(idPegawaiTujuansMultipart)
            multipartData.append(noSuratMultipart)
            multipartData.append(tglSuratMultipart)
            multipartData.append(sifatSuratMultipart)
            multipartData.append(perihalMultipart)
            multipartData.append(isiSuratMultipart)
            multipartData.append(dariSuratMultipart)
            multipartData.append(kepadaSuratMultipart)
    
            if fileLampiran != nil {
                let fileLampiranData = try! Data(contentsOf: fileLampiran!.filePath)
                let fileLampiranMultipart = MultipartFormData(provider: .data(fileLampiranData), name: "file_lampiran", fileName: fileLampiran!.displayName, mimeType: "*.*")
                multipartData.append(fileLampiranMultipart)
            }
            return .uploadMultipart(multipartData)
        case .sendDistribusiSuratKeluar(let userIdKoseptor, let userIdTtd, let idPegawaiTujuans,
                                        let userFile, let fileLampiran,
                                       let noSurat, let tglSurat, let sifatSurat, let perihal,
                                       let isiSurat, let dariSurat, let kepadaSurat):
            var multipartData = [MultipartFormData]()
            var sifatSuratId = sifatSurat.description
            let userIdKonseptorMultipart = MultipartFormData(provider: .data(userIdKoseptor.data(using: .utf8)!), name: "user_id_konseptor")
            let userIdTtdMultipart = MultipartFormData(provider: .data(userIdTtd.data(using: .utf8)!), name: "user_id_ttd")
            let idPegawaiTujuansMultipart = MultipartFormData(provider: .data(idPegawaiTujuans.data(using: .utf8)!), name: "id_pegawai_tujuans")
            let noSuratMultipart = MultipartFormData(provider: .data(noSurat.data(using: .utf8)!), name: "no_surat")
            let tglSuratMultipart = MultipartFormData(provider: .data(tglSurat.data(using: .utf8)!), name: "tgl_surat")
            let sifatSuratMultipart = MultipartFormData(provider: .data(sifatSuratId.data(using: .utf8)!), name: "sifat_surat")
            let perihalMultipart = MultipartFormData(provider: .data(perihal.data(using: .utf8)!), name: "perihal")
            let isiSuratMultipart = MultipartFormData(provider: .data(isiSurat.data(using: .utf8)!), name: "isi_surat")
            let dariSuratMultipart = MultipartFormData(provider: .data(dariSurat.data(using: .utf8)!), name: "dari_surat")
            let kepadaSuratMultipart = MultipartFormData(provider: .data(kepadaSurat.data(using: .utf8)!), name: "kepada_surat")
            
            let userFileData = try! Data(contentsOf: userFile.filePath)
            let userFileMultipart = MultipartFormData(provider: .data(userFileData), name: "userfile", fileName: userFile.displayName, mimeType: "*.*")
            
            multipartData.append(userFileMultipart)
            multipartData.append(userIdKonseptorMultipart)
            multipartData.append(userIdTtdMultipart)
            multipartData.append(idPegawaiTujuansMultipart)
            multipartData.append(noSuratMultipart)
            multipartData.append(tglSuratMultipart)
            multipartData.append(sifatSuratMultipart)
            multipartData.append(perihalMultipart)
            multipartData.append(isiSuratMultipart)
            multipartData.append(dariSuratMultipart)
            multipartData.append(kepadaSuratMultipart)
            
            if fileLampiran != nil {
                let fileLampiranData = try! Data(contentsOf: fileLampiran!.filePath)
                let fileLampiranMultipart = MultipartFormData(provider: .data(fileLampiranData), name: "file_lampiran", fileName: fileLampiran!.displayName, mimeType: "*.*")
                multipartData.append(fileLampiranMultipart)
            }
            return .uploadMultipart(multipartData)
        }
    }
    
    var headers: [String : String]? {
        return [:]
    }
}
