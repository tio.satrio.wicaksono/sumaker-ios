//
//  ParamInput.swift
//  ParamInput untuk mengisi parameter setiap API.
//  Setiap parameter bisa saja ada yang digunakan lebih dari satu API
//  Created by Satrio Wicaksono on 28/04/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import FileBrowser

class ParamInput{
   
    //MARK: API Login Parameter
    var username = ""
    var password = ""
    
    // MARK: API Get User Details Parameter
    var userId = ""
    
    //MARK: API Inbox/Outbox Mail Parameter
    var originId = ""
    var readStatus = ""
    var sifatId = ""
    var processStatus = ""
    var limitWaktuStatus = ""
    var offset = 0
    var numRow = 0
    var keyword = ""
    
    // MARK: API History Distribusi Surat (Inbox/Outbox)
    var mailId = ""
    
    // MARK: API Read Surat Status
    var status = ""
    
    // MARK: API Authentication token Parameter
    var key = ""
    
    // MARK: API Send Distribusi Surat Keluar / Masuk
    var userIdKonseptor = ""
    var userIdTtd = ""
    var idPegawaiTujuans = ""
    var noSurat = ""
    var tglSurat = ""
    var sifatSurat = 0
    var perihal = ""
    var isiSurat = ""
    var dariSurat = ""
    var kepadaSurat = ""
    var userFile : FBFile? = nil
    var fileLampiran : FBFile? = nil
    
    // MARK: API Agenda
    var bulan = ""
    var tahun = ""
}
