//
//  Api.swift
//  Pengaduan KLHK
//
//  Created by Satrio Wicaksono on 16/04/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import Moya

class Api {
    static let CODE_ERROR_INTERNAL = "log-runtime"
    static let MESSAGE_ERROR_INTERNAL = "Internal server error"
    static let BASE_URL_API = "https://sumaker.kemenkumham.go.id/api"
    static let BASE_URL_ESAKI = "https://sumaker.kemenkumham.go.id/esaki/ws"
    static let provider = MoyaProvider<ApiProvider>(plugins: [NetworkLoggerPlugin(verbose: true)])
    //static let provider = MoyaProvider<ApiProvider>()
    
    // MARK : Login
    static func requestLogin(paramInput: ParamInput, completion: @escaping (LoginModel) -> (), failure: @escaping (Error) -> ()) {
        provider.request(.login(username: paramInput.username, password: paramInput.password),
                         completion: { result in
                            switch result {
                            case .success(let response):
                                do {
                                    let model = try ResponseUtil<LoginModel>().parseFirst(response)
                                    completion(model)
                                } catch(let error) {
                                    failure(error)
                                }
                            case .failure(let err):
                                failure(err)
                            }
        })
    }
    
    // MARK : Get Profile Detail
    static func requestGetProfileDetail(paramInput: ParamInput, completion: @escaping (ProfileModel) -> (), failure: @escaping (Error) -> ()) {
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        paramInput.key = key
        provider.request(.getUserProfile(userId: paramInput.userId, key: paramInput.key), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<ProfileModel>().parseFirst(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Get Inbox Tab
    static func requestGetInboxTab(paramInput: ParamInput, completion: @escaping ([InboxTabModel]) -> (), failure: @escaping (Error) -> ()) {
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        paramInput.key = key
        provider.request(.getInboxTab(userId: paramInput.userId, key: paramInput.key), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<InboxTabModel>().parse(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Get Sifat Surat
    static func requestGetSifatSurat(paramInput: ParamInput, completion: @escaping ([SifatSuratModel]) -> (), failure: @escaping (Error) -> ()) {
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        paramInput.key = key
        provider.request(.getSifatSurat(key: paramInput.key), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<SifatSuratModel>().parse(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Get Surat Masuk
    static func requestGetSuratMasuk(paramInput: ParamInput, completion: @escaping ([SuratMasukModel]) -> (), failure: @escaping (Error) -> ()) {
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        paramInput.key = key
        provider.request(.getSuratMasuk(userId: paramInput.userId, originId: paramInput.originId, readStatus: paramInput.readStatus,
                                        sifatId: paramInput.sifatId, processStatus: paramInput.processStatus,
                                        limitWaktuStatus: paramInput.limitWaktuStatus, offset: paramInput.offset,
                                        numRow: paramInput.numRow, keyword: paramInput.keyword, key: paramInput.key), completion: { result in
                                            switch result {
                                            case .success(let response):
                                                do {
                                                    let model = try ResponseUtil<SuratMasukModel>().parse(response)
                                                    completion(model)
                                                }catch (let error) {
                                                    failure(error)
                                                }
                                            case .failure(let err):
                                                failure(err)
                                            }
        })
    }
    
    // MARK : Get Surat Keluar
    static func requestGetSuratKeluar(paramInput: ParamInput, completion: @escaping ([SuratKeluarModel]) -> (), failure: @escaping (Error) -> ()) {
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        paramInput.key = key
        provider.request(.getSuratKeluar(userId: paramInput.userId, originId: paramInput.originId, readStatus: paramInput.readStatus,
                                         sifatId: paramInput.sifatId, processStatus: paramInput.processStatus,
                                         limitWaktuStatus: paramInput.limitWaktuStatus, offset: paramInput.offset,
                                         numRow: paramInput.numRow, keyword: paramInput.keyword, key: paramInput.key), completion: { result in
                                            switch result {
                                            case .success(let response):
                                                do {
                                                    let model = try ResponseUtil<SuratKeluarModel>().parse(response)
                                                    completion(model)
                                                }catch (let error) {
                                                    failure(error)
                                                }
                                            case .failure(let err):
                                                failure(err)
                                            }
        })
    }
    
    // MARK : Get Surat Info History
    static func requestGetSuratInfoHistory(paramInput: ParamInput, completion: @escaping ([SuratInfoHistoryModel]) -> (), failure: @escaping (Error) -> ()) {
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        paramInput.key = key
        provider.request(.getSuratInfoHistory(mailId: paramInput.mailId, key: paramInput.key), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<SuratInfoHistoryModel>().parse(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Get Surat Info History
    static func requestGetDitujukanKepada(paramInput: ParamInput, completion: @escaping (DitujukanKepadaModel1) -> (), failure: @escaping (Error) -> ()) {
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        paramInput.key = key
        provider.request(.getDitujukanKepada(userId: paramInput.userId, key: paramInput.key), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<DitujukanKepadaModel1>().parseFirst(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Read Status Surat
    static func requestReadStatusSurat(paramInput: ParamInput, completion: @escaping (SuratStatusModel) -> (), failure: @escaping (Error) -> ()) {
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        paramInput.key = key
        provider.request(.readStatusSurat(mailId: paramInput.mailId, status: paramInput.status, key: paramInput.key), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<SuratStatusModel>().parseFirst(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Get Sifat Surat Distribusi
    static func requestGetSifatSuratDistribusi(paramInput: ParamInput, completion: @escaping ([SuratSifatDistribusiModel]) -> (), failure: @escaping (Error) -> ()) {
        //        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
        //            return
        //        }
        //        paramInput.key = key
        provider.request(.getSifatSuratDistribusi(userId: paramInput.userId), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<SuratSifatDistribusiModel>().parse(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Send Distribusi Surat Masuk
    static func requestSendDistribusiSuratMasuk(paramInput: ParamInput, completion: @escaping ([String]) -> (), failure: @escaping (Error) -> ()) {
        provider.request(.sendDistribusiSuratMasuk(userIdKoseptor: paramInput.userIdKonseptor, idPegawaiTujuans: paramInput.idPegawaiTujuans, userFile: paramInput.userFile!, fileLampiran: paramInput.fileLampiran ?? nil, noSurat: paramInput.noSurat, tglSurat: paramInput.tglSurat, sifatSurat: paramInput.sifatSurat, perihal: paramInput.perihal, isiSurat: paramInput.isiSurat, dariSurat: paramInput.dariSurat, kepadaSurat: paramInput.kepadaSurat), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<String>().parse(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Send Distribusi Surat Keluar
    static func requestSendDistribusiSuratKeluar(paramInput: ParamInput, completion: @escaping ([String]) -> (), failure: @escaping (Error) -> ()) {
        provider.request(.sendDistribusiSuratKeluar(userIdKoseptor: paramInput.userIdKonseptor, userIdTtd: paramInput.userIdTtd, idPegawaiTujuans: paramInput.idPegawaiTujuans, userFile: paramInput.userFile!, fileLampiran: paramInput.fileLampiran ?? nil, noSurat: paramInput.noSurat, tglSurat: paramInput.tglSurat, sifatSurat: paramInput.sifatSurat, perihal: paramInput.perihal, isiSurat: paramInput.isiSurat, dariSurat: paramInput.dariSurat, kepadaSurat: paramInput.kepadaSurat), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<String>().parse(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Total Unread Surat
    static func requestGetTotalUnread(paramInput: ParamInput, completion: @escaping (TotalUnreadModel) -> (), failure: @escaping (Error) -> ()) {
        provider.request(.getTotalUnread(userId: paramInput.userId), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<TotalUnreadModel>().parseFirst(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Get Read Inbox
    static func requestReadInbox(paramInput: ParamInput, completion: @escaping (InboxOutboxReadModel) -> (), failure: @escaping (Error) -> ()) {
        
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        
        paramInput.key = key
        provider.request(.getInboxRead(mailId: paramInput.mailId, key: key),
                         completion: { result in
                            switch result {
                            case .success(let response):
                                do {
                                    let model = try ResponseUtil<InboxOutboxReadModel>().parseFirst(response)
                                    completion(model)
                                } catch(let error) {
                                    failure(error)
                                }
                            case .failure(let err):
                                failure(err)
                        }
        })
    }
    
    // MARK : Get Read Outbox
    static func requestReadOutbox(paramInput: ParamInput, completion: @escaping (InboxOutboxReadModel) -> (), failure: @escaping (Error) -> ()) {
        
        guard let key = DBUtil.getInstance().getCurrentUser()?.token! else {
            return
        }
        
        paramInput.key = key
        provider.request(.getOutboxRead(mailId: paramInput.mailId, key: key),
                         completion: { result in
                            switch result {
                            case .success(let response):
                                do {
                                    let model = try ResponseUtil<InboxOutboxReadModel>().parseFirst(response)
                                    completion(model)
                                } catch(let error) {
                                    failure(error)
                                }
                            case .failure(let err):
                                failure(err)
                            }
        })
    }
    
    // MARK : Get Penandatangan
    static func requestPenandatangan(paramInput: ParamInput, completion: @escaping ([PenandatanganModel]) -> (), failure: @escaping (Error) -> ()) {
        
        provider.request(.getPenandatanganan(userId: paramInput.userId), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<PenandatanganModel>().parse(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }
    
    // MARK : Get Agenda
    static func requestAgenda(paramInput: ParamInput, completion: @escaping ([AgendaModel]) -> (), failure: @escaping (Error) -> ()) {
        
        provider.request(.getAgenda(userId: paramInput.userId, bulan: paramInput.bulan, tahun: paramInput.tahun), completion: { result in
            switch result {
            case .success(let response):
                do {
                    let model = try ResponseUtil<AgendaModel>().parse(response)
                    completion(model)
                }catch (let error) {
                    failure(error)
                }
            case .failure(let err):
                failure(err)
            }
        })
    }

}

enum ApiError: Error {
    case internalServerError
    case fromResponse(errorModel: ErrorModel)
}

extension ApiError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .internalServerError:
            return "Internal Server Error"
        case .fromResponse(let errorModel):
            return errorModel.detail
        }
    }
}

