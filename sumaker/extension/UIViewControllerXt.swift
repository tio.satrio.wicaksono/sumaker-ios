//
//  UIViewControllerXt.swift
//
//  Created by Satrio Wicaksono on 18/04/18.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func setEnableViews(enable: Bool, _ views: UIView...) {
        views.forEach({ $0.isUserInteractionEnabled = enable })
    }
    
    func setHiddenViews(hidden: Bool, _ views: UIView...) {
        views.forEach({ $0.isHidden = hidden })
    }

    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func setTitle(_ title: String) {
        self.navigationItem.title = title
    }
    
    func showAlert(title: String?, message: String?, actions: [UIAlertAction], _ completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { action in
            alert.addAction(action)
        }
        present(alert, animated: true, completion: completion)
    }
    
    func showError(title: String?, message: String, positiveText: String = "OK", handler: ((UIAlertAction) -> Void)? = nil) {
        let actionPositive = UIAlertAction(title: positiveText, style: .default, handler: handler)
        showAlert(title: title, message: message, actions: [actionPositive])
    }
    
    func showConfirmationDialog(title: String?, message: String, buttons: [String], handlers: [((UIAlertAction) -> Void)?]) {
        let actionNegative = UIAlertAction(title: buttons[0], style: .cancel, handler: handlers[0])
        let actionPositive = UIAlertAction(title: buttons[1], style: .default, handler: handlers[1])
        showAlert(title: title, message: message, actions: [actionPositive, actionNegative])
    }
    
    
    // MARK: -
    
    func resetPickerView(_ pickerView: UIPickerView) {
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
    }
    
    func resetTextField(_ textField: UITextField) {
        textField.text = ""
    }
    
    func resetPickerViews(_ pickerViews: [UIPickerView]) {
        pickerViews.forEach { it in
            resetPickerView(it)
        }
    }
    
    func resetTextFields(_ textFields: [UITextField]) {
        textFields.forEach { it in
            resetTextField(it)
        }
    }
    
    func pair(input textField: UITextField, with pickerView: UIPickerView) {
        pickerView.delegate = self as? UIPickerViewDelegate
        pickerView.dataSource = self as? UIPickerViewDataSource
        textField.inputView = pickerView
    }
    
}


// MARK: - Activity Indicator related

extension UIViewController {
    
    private static let fullscreenActivityIndicatorViewTag = 88
    
    var isNetworkActivityIndicatorVisible: Bool {
        set {
            UIApplication.shared.isNetworkActivityIndicatorVisible = newValue
        }
        get {
            return UIApplication.shared.isNetworkActivityIndicatorVisible
        }
    }
    
    var isFullscreenActivityIndicatorVisible: Bool {
        set {
            var indicatorView = view.viewWithTag(UIViewController.fullscreenActivityIndicatorViewTag) as? UIActivityIndicatorView
            if newValue {
                if indicatorView == nil {
                    indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                    indicatorView?.tag = UIViewController.fullscreenActivityIndicatorViewTag
                }
                indicatorView?.frame = self.view.bounds
                indicatorView?.backgroundColor = UIColor(white: 0, alpha: 0.66)
                indicatorView?.startAnimating()
                indicatorView?.isHidden = false
                self.view.addSubview(indicatorView!)
                self.view.isUserInteractionEnabled = false
            } else {
                if indicatorView != nil {
                    indicatorView?.stopAnimating()
                    indicatorView?.removeFromSuperview()
                }
                self.view.isUserInteractionEnabled = true
            }
        }
        get {
            let indicatorView = view.viewWithTag(UIViewController.fullscreenActivityIndicatorViewTag) as? UIActivityIndicatorView
            return (indicatorView != nil) && !indicatorView!.isHidden
        }
    }
    
}
