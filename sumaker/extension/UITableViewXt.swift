//
//  UITableViewXt.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 11/09/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func indexPathsForRowsInSection(_ section: Int) -> [IndexPath] {
        return (0..<self.numberOfRows(inSection: section)).map { IndexPath(row: $0, section: section) }
    }
}
