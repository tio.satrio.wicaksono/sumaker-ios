//
//  UIViewXt.swift
//  Sumaker
//
//  Created by Satrio Wicaksono on 09/05/18.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import UIKit

extension UIView {
    
    func useStandardShadow() {
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.cornerRadius = 2.0
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 1.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
    }
    
    func useDeepShadow() {
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.cornerRadius = 2.0
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 1.0
        layer.shadowOpacity = 1.0
        layer.masksToBounds = false
    }
    
    func setCircleCorner() {
        self.layer.cornerRadius = min(self.frame.size.height, self.frame.size.width) / 2.0
    }
    
}
