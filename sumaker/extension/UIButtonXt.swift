//
//  UIButtonXt.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 26/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    func setSifatColor(sifat: String){
        switch sifat {
        case SifatSurat.BIASA:
            self.backgroundColor = UIColor(red: 50/255, green: 136/255, blue: 170/255, alpha: 1.0)
        case SifatSurat.PENTING:
            self.backgroundColor = UIColor(red: 197/255, green: 27/255, blue: 245/255, alpha: 1.0)
        case SifatSurat.SEGERA:
            self.backgroundColor = UIColor(red: 252/255, green: 146/255, blue: 52/255, alpha: 1.0)
        case SifatSurat.SANGAT_SEGERA:
            self.backgroundColor = UIColor(red: 189/255, green: 73/255, blue: 74/255, alpha: 1.0)
        case SifatSurat.RAHASIA:
            self.backgroundColor = UIColor.black
        default:
            self.backgroundColor = UIColor.black
        }
        self.setTitle(sifat, for: .normal)
    }
    
    func setStatusColor(status: String){
        
        switch status {
        case StatusSurat.BELUM_BACA:
            self.backgroundColor = UIColor(red: 209/255, green: 206/255, blue: 31/255, alpha: 1.0)
        case StatusSurat.SUDAH_BACA:
            self.backgroundColor = UIColor(red: 42/255, green: 178/255, blue: 58/255, alpha: 1.0)
        case StatusSurat.BELUM_PROSES:
            self.backgroundColor = UIColor(red: 137/255, green: 22/255, blue: 175/255, alpha: 1.0)
        case StatusSurat.SUDAH_PROSES:
            self.backgroundColor = UIColor(red: 19/255, green: 189/255, blue: 219/255, alpha: 1.0)
        default:
            self.backgroundColor = UIColor.black
        }
        self.setTitle(status, for: .normal)
    }
}

struct SifatSurat {
    static let SANGAT_SEGERA = "Sangat Segera"
    static let BIASA = "Biasa"
    static let PENTING = "Penting"
    static let SEGERA = "Segera"
    static let RAHASIA = "Rahasia"
}

struct StatusSurat {
    static let BELUM_BACA = "Belum Dibaca"
    static let SUDAH_BACA = "Sudah Dibaca"
    static let BELUM_PROSES = "Belum Diproses"
    static let SUDAH_PROSES = "Sudah Diproses"
}

struct StatusSuratParam {
    static let ALL = "all"
    static let READ = "read"
    static let UNREAD = "unread"
    static let PROCESSED = "processed"
    static let UNPROCESS = "unprocess"
    static let ONPROGRESS = "onprogress"
    static let OVERLIMIT = "overlimit"
}
