//
//  UITextViewXt.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 20/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    func setGeneralStyle(){
        self.layer.borderWidth = 0.25
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 5
    }
}
