//
//  CGFloatXt.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 20/12/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
