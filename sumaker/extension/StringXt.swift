//
//  StringXt.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 20/12/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

extension String {
   
    func parseDateTimeToDate() -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fromDate = dateFormatterGet.date(from: self)
        
        let dateFormatterParse = DateFormatter()
        dateFormatterParse.dateFormat = "yyyy-MM-dd"
        return dateFormatterParse.string(from: fromDate!)
    }
    
    func parseToDate() -> Date{
        let dateFormatterParse = DateFormatter()
        dateFormatterParse.dateFormat = "yyyy-MM-dd"
        return dateFormatterParse.date(from: self)!
    }
}
