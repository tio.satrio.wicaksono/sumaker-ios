//
//  DateXt.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 23/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

extension Date {
   
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
    
    func parseToStringWithFormat(format: String = "yyyy-MM-dd") -> String{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = format
            return dateFormatter.string(from: self)
    }
}
