//
//  LoginViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 22/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit
import SwiftHash

class LoginViewController: BaseViewController<LoginPresenter>, LoginDelegate{
    
    @IBOutlet weak var inputUsername: UITextField!
    @IBOutlet weak var inputPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = LoginPresenter(view: self)
        
        let fibonacci = getFibonacci(n: 6)
        print("Fibonacci : \(fibonacci)")
        // 0, 1, 1, 2, 3, 5, 8
        
        let isResultPalindrom = isPalindrom(text: "siss")
        print("Is Palindrom: \(isResultPalindrom)")
        
        let isResultAnagram = isAnagram(text1: "eat", text2: "ate")
        print("Is Anagram: \(isResultAnagram)")
        
        var a = [[1, 2], [3, 4], [5, 6], [7, 8], [9, 10]]
        var b = a.flatMap { $0 }.filter { $0 % 2 == 1 };
        print(a)
        print(b)
    }
    
    func getFibonacci(n: Int) -> Int{
        if n < 2 && n > 0 {
            return 1
        }
        if n <= 0 {
            return 0
        }
        return getFibonacci(n: n - 2) + getFibonacci(n: n - 1)
    }
    
    func isPalindrom(text: String) -> Bool{
        let arrOfText = Array(text.lowercased())
        for i in 0 ..< arrOfText.count {
            if arrOfText[i] != arrOfText[text.count - 1 - i] {
                return false
            }
        }
        return true
    }
    
    func isAnagram(text1: String, text2: String) -> Bool{
        let arrText1 = Array(text1.lowercased()).sorted()
        let arrText2 = Array(text2.lowercased()).sorted()
        let text1Arr = String(arrText1)
        let text2Arr = String(arrText2)
        print("Text 1: \(text1Arr)")
        print("Text 2: \(text2Arr)")
        if arrText1 == arrText2 {
            return true
        } else {
            return false
        }
        // eat
        // ate
    }
    
    
    @IBAction func btnLoginTapped(_ sender: Any) {
        proccessInput()
    }
    
    func didSuccessLogin(loginModel: LoginModel) {
        DBUtil.getInstance().saveUserIdentity(loginModel: loginModel)
        goToMain()
    }
    
    func taskDidBegin() {
        isFullscreenActivityIndicatorVisible = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func taskDidFinish() {
        isFullscreenActivityIndicatorVisible = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
    private func proccessInput(){
        let validator = ValidatorUtil.getInstance()
        do{
            inputUsername.text = "kasubidpti"
            inputPassword.text = "*un1x*"
//            inputUsername.text = "kapusdatin"
//            inputPassword.text = "rtyu"
            let username = try validator.validateText(text: inputUsername.text!, type: nil, name: "Username")
            let password = try validator.validateText(text: inputPassword.text!, type: nil, name: "Password")
            let paramInput = ParamInput()
            paramInput.username = username
            paramInput.password = MD5(password)
            presenter.login(paramInput: paramInput)
        }catch ValidatorError.error(let reason){
            showError(title: "Pesan", message: reason)
        }catch{
            showError(title: "Pesan", message: error.localizedDescription)
        }
    }
    
    private func goToMain(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        present(viewController, animated: true, completion: nil)
    }
}

