//
//  LoginPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 22/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class LoginPresenter: BasePresenter<LoginDelegate>{
    
    func login(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestLogin(paramInput: paramInput, completion: { model in
            self.view.didSuccessLogin(loginModel: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
}

protocol LoginDelegate: BaseDelegate {
    func didSuccessLogin(loginModel: LoginModel)
}
