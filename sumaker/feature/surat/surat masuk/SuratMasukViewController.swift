//
//  ViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 22/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import UIKit
import DeepDiff
import DropDown

class SuratMasukViewController: BaseViewController<SuratPresenter>,
UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource,
UITableViewDelegate, UITableViewDataSource, InboxTabDelegate,
SifatSuratDelegate, SuratDelegate{
    
    @IBOutlet weak var inputSuratDari: UITextField!
    @IBOutlet weak var inputKategoriSurat: UITextField!
    @IBOutlet weak var inputCari: UITextField!
    @IBOutlet weak var tblSurat: UITableView!
    var bbiRight : UIBarButtonItem!
    let refresher = UIRefreshControl()
    let pvSuratDari = UIPickerView()
    let pvKategoriSurat = UIPickerView()
    let ddReadType = DropDown()
    let paramInput = ParamInput()
    var listSuratDari = Array<InboxTabModel>()
    var listKategoriSurat = Array<SifatSuratModel>()
    var listSurat = Array<SuratMasukModel>()
    var inboxTabPresenter: InboxTabPresenter!
    var sifatSuratPresenter: SifatSuratPresenter!
    var selectedSuratMasuk: SuratMasukModel!
    let readTypeItems = ["All", "Read", "Unread"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInputView()
        setTableView()
        setDelegate()
        setDataSource()
        setPresenter()
        setDropDownReadType()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setDropDownReadType(){
        bbiRight = UIBarButtonItem(title: readTypeItems[0], style: .plain, target: self, action: #selector(didOpenReadType))
        ddReadType.width = 150
        ddReadType.dataSource = readTypeItems
        ddReadType.anchorView = bbiRight
        navigationItem.rightBarButtonItem = bbiRight
    }
    
    private func setTableView() {
        refresher.addTarget(self, action: #selector(didRefresh), for: .valueChanged)
        tblSurat.addSubview(refresher)
        tblSurat.register(UINib(nibName: "SuratViewCell", bundle: nil), forCellReuseIdentifier: "SuratViewCell")
    }
    
    private func setDelegate(){
        inputCari.delegate = self
        pvSuratDari.delegate = self
        pvKategoriSurat.delegate = self
        tblSurat.delegate = self
    }
    
    private func setDataSource(){
        pvSuratDari.dataSource = self
        pvKategoriSurat.dataSource = self
        tblSurat.dataSource = self
    }
    
    private func setInputView(){
        inputSuratDari.inputView = pvSuratDari
        inputKategoriSurat.inputView = pvKategoriSurat
    }
    
    private func setPresenter(){
        paramInput.userId = DBUtil.getInstance().getCurrentUser()!.userId!
        inboxTabPresenter = InboxTabPresenter(view: self)
        inboxTabPresenter.getInboxTab(paramInput: paramInput)
        sifatSuratPresenter = SifatSuratPresenter(view: self)
        presenter = SuratPresenter(view: self)
    }
    
    private func initRequestSuratMasuk() {
        paramInput.userId = DBUtil.getInstance().getCurrentUser()!.userId!
        paramInput.originId = listSuratDari[pvSuratDari.selectedRow(inComponent: 0)].id
        paramInput.sifatId = listKategoriSurat[pvKategoriSurat.selectedRow(inComponent: 0)].id
        paramInput.offset = 0
        paramInput.numRow = 5
        paramInput.readStatus = StatusSuratParam.ALL
        paramInput.processStatus = StatusSuratParam.ALL
        paramInput.limitWaktuStatus = StatusSuratParam.ALL
        presenter.getSuratMasuk(paramInput: paramInput)
    }
    
    private func processViewCell(cell: SuratViewCell, model: SuratMasukModel) -> SuratViewCell{
        
        cell.lblJudul.text = model.perihal
        cell.lblAsalSurat.text = model.asalSurat
        cell.lblWaktu.text = model.tglDikirim
        cell.lblNama.text = model.namaPengirim
        cell.btnStatusBaca.setStatusColor(status: model.statusDibaca)
        cell.btnSifat.setSifatColor(sifat: model.sifatSurat)
        if model.statusDiproses != nil {
            cell.btnStatusProses.setStatusColor(status: model.statusDiproses!)
        } else {
            cell.btnStatusProses.isHidden = true
        }
        
        return cell
    }
    
    private func resetTable() {
        paramInput.offset = 0
        listSurat.removeAll()
        tblSurat.reloadData()
    }
    
    private func loadMore(index: IndexPath){
        if index.row == listSurat.count - 1 {
            paramInput.offset = listSurat.count
            presenter.getSuratMasuk(paramInput: paramInput)
        }
    }
    
    @objc func didOpenReadType(){
        ddReadType.show()
        ddReadType.selectionAction = { [weak self] (index, item) in
            self?.bbiRight.title = self?.readTypeItems[index]
            self?.paramInput.readStatus = (self?.bbiRight.title?.lowercased())!
            self?.resetTable()
            self?.presenter.getSuratMasuk(paramInput: (self?.paramInput)!)
        }
    }
    
    @objc func didRefresh(){
        resetTable()
        presenter.getSuratMasuk(paramInput: paramInput)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count: Int!
        if pickerView == pvKategoriSurat {
            count = listKategoriSurat.count
        } else if pickerView == pvSuratDari {
            count = listSuratDari.count
        }
        return count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var titleRow: String!
        if pickerView == pvSuratDari {
            titleRow = listSuratDari[row].label
        } else if pickerView == pvKategoriSurat {
           titleRow = listKategoriSurat[row].label
        }
        return titleRow
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pvSuratDari {
            inputSuratDari.text = listSuratDari[row].label
            paramInput.originId = listSuratDari[row].id
        } else if pickerView == pvKategoriSurat {
            inputKategoriSurat.text = listKategoriSurat[row].label
            paramInput.sifatId = listKategoriSurat[row].id
        }
        resetTable()
        dismissKeyboard()
        presenter.getSuratMasuk(paramInput: paramInput)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == inputCari {
            paramInput.keyword = textField.text!
            resetTable()
            presenter.getSuratMasuk(paramInput: paramInput)
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSurat.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = listSurat[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuratViewCell", for: indexPath) as! SuratViewCell
        cell.row = indexPath.row
        cell.tapped = {(cell, row) in
            self.selectedSuratMasuk = model
            self.paramInput.mailId = model.id
            self.paramInput.status = "1"
            self.presenter.readStatusSurat(paramInput: self.paramInput)
        }
        loadMore(index: indexPath)
        return processViewCell(cell: cell, model: model)
    }
    
    func didLoadInboxTab(list: Array<InboxTabModel>) {
        listSuratDari.removeAll()
        listSuratDari.append(contentsOf: list)
        pvSuratDari.selectRow(0, inComponent: 0, animated: true)
        inputSuratDari.text = listSuratDari[0].label
        sifatSuratPresenter.getSifatSurat(paramInput: paramInput)
    }
    
    func didLoadSifatSurat(list: Array<SifatSuratModel>) {
        listKategoriSurat.removeAll()
        listKategoriSurat.append(contentsOf: list)
        pvKategoriSurat.selectRow(0, inComponent: 0, animated: true)
        inputKategoriSurat.text = listKategoriSurat[0].label
        initRequestSuratMasuk()
    }
    
    func didLoadSuratMasuk(list: [SuratMasukModel]) {
        var currentIndex = listSurat.count
        tblSurat.beginUpdates()
        list.forEach { (surat) in
            listSurat.insert(surat, at: currentIndex)
            tblSurat.insertRows(at: [IndexPath(row: currentIndex, section: 0)], with: .automatic)
            currentIndex = currentIndex + 1
        }
        tblSurat.endUpdates()
    }
    
    func didReadStatusSurat(model: SuratStatusModel) {
        if model.success == "1" {
            let viewController = self.getViewController(storyboardName: "Main", identifier: "SuratDetailMasukViewController") as! SuratDetailMasukViewController
            viewController.navigationItem.title = "Surat Masuk"
            viewController.suratMasukModel = selectedSuratMasuk
            let navController = UINavigationController(rootViewController: viewController)
            self.present(navController, animated: true, completion: nil)
        } else {
            showError(title: "Error", message: "Gagal membuka surat")
        }
    }

    func taskDidBegin() {
        refresher.beginRefreshing()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func taskDidFinish() {
        refresher.endRefreshing()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
}

