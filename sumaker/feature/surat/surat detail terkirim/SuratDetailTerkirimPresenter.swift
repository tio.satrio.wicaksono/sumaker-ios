//
//  SuratDetailTerkirimPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 24/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class SuratDetailTerkirimPresenter: BasePresenter<SuratDetailTerkirimDelegate>{
    func outboxRead(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestReadOutbox(paramInput: paramInput, completion: { model in
            self.view.didSuccessLoad(outboxReadModel: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
}

protocol SuratDetailTerkirimDelegate: BaseDelegate {
    func didSuccessLoad(outboxReadModel: InboxOutboxReadModel)
}
