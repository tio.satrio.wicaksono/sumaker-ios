//
//  SuratDetailTerkirimViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 24/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class SuratDetailTerkirimViewController: BaseViewController<SuratDetailTerkirimPresenter>,
SuratDetailTerkirimDelegate{
    
    var suratKeluarModel: SuratKeluarModel!
    var inboxReadModel: InboxOutboxReadModel!
    var paramInput: ParamInput!
    
    @IBOutlet weak var lblNamaDari: UILabel!
    @IBOutlet weak var lblSuratKepada: UILabel!
    @IBOutlet weak var lblTglSurat: UILabel!
    @IBOutlet weak var lblSifatSurat: UILabel!
    @IBOutlet weak var lblPerihal: UILabel!
    @IBOutlet weak var lblRingkasanSurat: UILabel!
    @IBOutlet weak var lblCatatanDisposisi: UILabel!
    @IBOutlet weak var btnDownloadFileSurat: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
    }
    
    private func initParamInput(){
        paramInput = ParamInput()
        paramInput.mailId = suratKeluarModel.id
    }
    
    private func initPresenter(){
        initParamInput()
        presenter = SuratDetailTerkirimPresenter(view: self)
        presenter.outboxRead(paramInput: paramInput)
    }
    
    private func initContent(model: InboxOutboxReadModel){
        inboxReadModel = model
        lblNamaDari.text = model.namaPegawaiKe
        lblSuratKepada.text = model.suratKepada
        lblPerihal.text = model.perihal
        lblTglSurat.text = model.tglSurat
        lblSifatSurat.text = model.sifatSurat
        lblRingkasanSurat.text = model.ringkasanSurat
        lblCatatanDisposisi.text = model.catatanDisposisi
        if model.fileSurat != nil {
            btnDownloadFileSurat.isHidden = false
            btnDownloadFileSurat.addTarget(self, action: #selector(onDownloadClicked), for: .touchUpInside)
        } else {
            btnDownloadFileSurat.isHidden = true
        }
    }
    
    @objc func onDownloadClicked(){
        let urlFile = URL(string: self.inboxReadModel.fileSurat!)
        UIApplication.shared.open(urlFile!, options: [:])
        //        DownloadUtil.getInstance().download(url: inboxReadModel.fileSurat!, delegates: self)
    }
    
    func taskDidBegin() {
        isFullscreenActivityIndicatorVisible = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didSuccessLoad(outboxReadModel: InboxOutboxReadModel) {
        initContent(model: outboxReadModel)
    }
    
    func taskDidFinish() {
        isFullscreenActivityIndicatorVisible = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
    @IBAction func tappedBackNavigation(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedHistorySurat(_ sender: Any) {
        let viewController = getViewController(storyboardName: "Main", identifier: "SuratHistoryViewController") as! SuratHistoryViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func tappedSuratInfo(_ sender: Any) {
        let viewController = getViewController(storyboardName: "Main", identifier: "SuratInfoViewController") as! SuratInfoViewController
        viewController.title = "Histori Surat"
        viewController.mailId = suratKeluarModel.id
        navigationController?.pushViewController(viewController, animated: true)
    }
}
