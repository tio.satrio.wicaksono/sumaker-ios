//
//  SuratDisposisiViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 24/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit
import UICheckbox_Swift

class SuratDisposisiViewController: BaseViewController<SuratDisposisiPresenter>,
UITextFieldDelegate, RetrieveSelectedDelegate{
    
    @IBOutlet weak var inputDitujukanKepada: UITextField!
    @IBOutlet weak var inputDisposisiCatatan: UITextView!
    
    @IBOutlet weak var cbJadwalkan: UICheckbox!
    @IBOutlet weak var cbMenugaskan: UICheckbox!
    @IBOutlet weak var cbTindakLanjut: UICheckbox!
    @IBOutlet weak var cbEdarkan: UICheckbox!
    @IBOutlet weak var cbProses: UICheckbox!
    @IBOutlet weak var cbUntukDiketahui: UICheckbox!
    @IBOutlet weak var cbSiapkanBahan: UICheckbox!
    @IBOutlet weak var cbUntukDikoreksi: UICheckbox!
    @IBOutlet weak var cbHadiri: UICheckbox!
    @IBOutlet weak var cbArsip: UICheckbox!
    
    var listDitujukanKepada = Array<DitujukanKepadaModel>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDelegate()
        initTextViewStyle()
        initCheckbox()
    }
    
    private func initDelegate() {
        inputDitujukanKepada.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let viewController = getViewController(storyboardName: "Main", identifier: "DitujukanKepadaViewController") as! DitujukanKepadaViewController
        let navController = UINavigationController(rootViewController: viewController)
        viewController.retrieveSelectedDelegate = self
        viewController.listCurrent = listDitujukanKepada
        present(navController, animated: true, completion: nil)
        return true
    }
    
    func onRetrieveSelectedData(list: [ChildrenDitujukanKepada]) {
//        print("Jumlah \(list.count)")
//        var selectedData = ""
//        listDitujukanKepada.removeAll()
//        listDitujukanKepada.append(contentsOf: list)
//        listDitujukanKepada.forEach { (model) in
//            selectedData = selectedData + ";" + model.jabatan
//        }
//        if selectedData.count > 0 {
//            selectedData.removeFirst()
//            selectedData.removeLast()
//        }
//        inputDitujukanKepada.text = selectedData
    }
    
    private func initTextViewStyle(){
        inputDisposisiCatatan.setGeneralStyle()
    }
    
    private func initCheckbox() {
        cbJadwalkan.onSelectStateChanged = {(checkbox, selected) in }
        cbMenugaskan.onSelectStateChanged = {(checkbox, selected) in }
        cbTindakLanjut.onSelectStateChanged = {(checkbox, selected) in }
        cbEdarkan.onSelectStateChanged = {(checkbox, selected) in }
        cbProses.onSelectStateChanged = {(checkbox, selected) in }
        cbUntukDiketahui.onSelectStateChanged = {(checkbox, selected) in }
        cbSiapkanBahan.onSelectStateChanged = {(checkbox, selected) in }
        cbUntukDikoreksi.onSelectStateChanged = {(checkbox, selected) in }
        cbHadiri.onSelectStateChanged = {(checkbox, selected) in }
        cbArsip.onSelectStateChanged = {(checkbox, selected) in }
    }
}
