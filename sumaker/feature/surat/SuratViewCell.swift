//
//  SuratViewCell.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 26/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class SuratViewCell: UITableViewCell{
    typealias Action = (SuratViewCell, Int) -> Void
    
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var lblJudul: UILabel!
    @IBOutlet weak var lblWaktu: UILabel!
    @IBOutlet weak var btnStatusBaca: UIButton!
    @IBOutlet weak var btnStatusProses: UIButton!
    @IBOutlet weak var lblAsalSurat: UILabel!
    @IBOutlet weak var btnSifat: UIButton!
    var tapped: Action?
    var row: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapped)))
        row = 0
    }
    
    @objc func onTapped() {
        tapped?(self, row)
    }
    
}
