//
//  MainPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 22/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class SuratPresenter: BasePresenter<SuratDelegate>{
    
    func getSuratMasuk(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestGetSuratMasuk(paramInput: paramInput, completion: { model in
            self.view.didLoadSuratMasuk(list: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
    
    func getSuratKeluar(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestGetSuratKeluar(paramInput: paramInput, completion: { model in
            self.view.didLoadSuratKeluar(list: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
    
    func readStatusSurat(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestReadStatusSurat(paramInput: paramInput, completion: { (model) in
            self.view.didReadStatusSurat(model: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
}

protocol SuratDelegate: BaseDelegate{
    func didLoadSuratMasuk(list: [SuratMasukModel])
    func didLoadSuratKeluar(list: [SuratKeluarModel])
    func didReadStatusSurat(model: SuratStatusModel)
}

extension SuratDelegate {
    func didLoadSuratMasuk(list: [SuratMasukModel]){}
    func didLoadSuratKeluar(list: [SuratKeluarModel]){}
    func didReadStatusSurat(model: SuratStatusModel){}
}
