//
//  SuratTerkirimViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 24/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class SuratTerkirimViewController: BaseViewController<SuratPresenter>,
    UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource,
UITableViewDelegate, UITableViewDataSource, InboxTabDelegate,
SifatSuratDelegate, SuratDelegate{
    
    @IBOutlet weak var inputSuratDari: UITextField!
    @IBOutlet weak var inputKategoriSurat: UITextField!
    @IBOutlet weak var inputCari: UITextField!
    @IBOutlet weak var tblSurat: UITableView!
    let refresher = UIRefreshControl()
    let paramInput = ParamInput()
    let pvSuratDari = UIPickerView()
    let pvKategoriSurat = UIPickerView()
    var listSuratDari = Array<InboxTabModel>()
    var listKategoriSurat = Array<SifatSuratModel>()
    var listSurat = Array<SuratKeluarModel>()
    var inboxTabPresenter: InboxTabPresenter!
    var sifatSuratPresenter: SifatSuratPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInputView()
        setTableView()
        setDelegate()
        setDataSource()
        setPresenter()
    }
    
    private func setTableView(){
        refresher.addTarget(self, action: #selector(didRefresh), for: .valueChanged)
        tblSurat.addSubview(refresher)
        tblSurat.register(UINib(nibName: "SuratViewCell", bundle: nil), forCellReuseIdentifier: "SuratViewCell")
    }
    
    private func setDelegate(){
        inputCari.delegate = self
        pvSuratDari.delegate = self
        pvKategoriSurat.delegate = self
        tblSurat.delegate = self
    }
    
    private func setDataSource(){
        pvSuratDari.dataSource = self
        pvKategoriSurat.dataSource = self
        tblSurat.dataSource = self
    }
    
    private func setInputView(){
        inputSuratDari.inputView = pvSuratDari
        inputKategoriSurat.inputView = pvKategoriSurat
    }
    
    private func setPresenter(){
        paramInput.userId = DBUtil.getInstance().getCurrentUser()!.userId!
        inboxTabPresenter = InboxTabPresenter(view: self)
        inboxTabPresenter.getInboxTab(paramInput: paramInput)
        sifatSuratPresenter = SifatSuratPresenter(view: self)
        presenter = SuratPresenter(view: self)
    }
    
    private func initRequestSuratMasuk() {
        paramInput.userId = DBUtil.getInstance().getCurrentUser()!.userId!
        paramInput.originId = listSuratDari[pvSuratDari.selectedRow(inComponent: 0)].id
        paramInput.sifatId = listKategoriSurat[pvKategoriSurat.selectedRow(inComponent: 0)].id
        paramInput.offset = 0
        paramInput.numRow = 5
        paramInput.readStatus = StatusSuratParam.ALL
        paramInput.processStatus = StatusSuratParam.ALL
        paramInput.limitWaktuStatus = StatusSuratParam.ALL
        presenter.getSuratKeluar(paramInput: paramInput)
    }
    
    private func processViewCell(cell: SuratViewCell, model: SuratKeluarModel) -> SuratViewCell{
        
        cell.lblJudul.text = model.perihal
        cell.lblAsalSurat.text = model.asalSurat
        cell.lblWaktu.text = model.tglDikirim
        cell.lblNama.text = model.namaPenerima
        cell.btnStatusBaca.setStatusColor(status: model.statusDibaca)
        cell.btnSifat.setSifatColor(sifat: model.sifatSurat)
        if model.statusDiproses != nil {
            cell.btnStatusProses.setStatusColor(status: model.statusDiproses!)
        } else {
            cell.btnStatusProses.isHidden = true
        }
        
        return cell
    }
    
    private func resetTable() {
        paramInput.offset = 0
        listSurat.removeAll()
        tblSurat.reloadData()
    }
    
    private func loadMore(index: IndexPath){
        if index.row == listSurat.count - 1 {
            paramInput.offset = listSurat.count
            presenter.getSuratKeluar(paramInput: paramInput)
        }
    }
    
    @objc func didRefresh(){
        resetTable()
        presenter.getSuratKeluar(paramInput: paramInput)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count: Int!
        if pickerView == pvKategoriSurat {
            count = listKategoriSurat.count
        } else if pickerView == pvSuratDari {
            count = listSuratDari.count
        }
        return count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var titleRow: String!
        if pickerView == pvSuratDari {
            titleRow = listSuratDari[row].label
        } else if pickerView == pvKategoriSurat {
            titleRow = listKategoriSurat[row].label
        }
        return titleRow
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pvSuratDari {
            inputSuratDari.text = listSuratDari[row].label
            paramInput.originId = listSuratDari[row].id
        } else if pickerView == pvKategoriSurat {
            inputKategoriSurat.text = listKategoriSurat[row].label
            paramInput.sifatId = listKategoriSurat[row].id
        }
        resetTable()
        dismissKeyboard()
        presenter.getSuratKeluar(paramInput: paramInput)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == inputCari {
            resetTable()
            paramInput.keyword = textField.text!
            presenter.getSuratKeluar(paramInput: paramInput)
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSurat.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = listSurat[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuratViewCell", for: indexPath) as! SuratViewCell
        cell.row = indexPath.row
        cell.tapped = {(cell, row) in
            let viewController = self.getViewController(storyboardName: "Main", identifier: "SuratDetailTerkirimViewController") as! SuratDetailTerkirimViewController
            viewController.navigationItem.title = "Surat Terkirim"
            viewController.suratKeluarModel = model
            let navController = UINavigationController(rootViewController: viewController)
            self.present(navController, animated: true, completion: nil)
        }
        loadMore(index: indexPath)
        return processViewCell(cell: cell, model: model)
    }
    
    func didLoadInboxTab(list: Array<InboxTabModel>) {
        listSuratDari.removeAll()
        listSuratDari.append(contentsOf: list)
        pvSuratDari.selectRow(0, inComponent: 0, animated: true)
        inputSuratDari.text = listSuratDari[0].label
        sifatSuratPresenter.getSifatSurat(paramInput: paramInput)
    }
    
    func didLoadSifatSurat(list: Array<SifatSuratModel>) {
        listKategoriSurat.removeAll()
        listKategoriSurat.append(contentsOf: list)
        pvKategoriSurat.selectRow(0, inComponent: 0, animated: true)
        inputKategoriSurat.text = listKategoriSurat[0].label
        initRequestSuratMasuk()
    }
    
    func didLoadSuratKeluar(list: [SuratKeluarModel]) {
        var currentIndex = listSurat.count
        tblSurat.beginUpdates()
        list.forEach { (surat) in
            listSurat.insert(surat, at: currentIndex)
            tblSurat.insertRows(at: [IndexPath(row: currentIndex, section: 0)], with: .automatic)
            currentIndex = currentIndex + 1
        }
        tblSurat.endUpdates()
    }
    
    func taskDidBegin() {
        refresher.beginRefreshing()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func taskDidFinish() {
        refresher.endRefreshing()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
}
