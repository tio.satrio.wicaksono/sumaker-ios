//
//  SuratHistoryViewCell.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 16/07/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class SuratHistoryViewCell: UITableViewCell{
    @IBOutlet weak var ivSuratHistory: UIImageView!
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var lblKepada: UILabel!
    @IBOutlet weak var lblWaktu: UILabel!
    @IBOutlet weak var lblTindakan: UILabel!
    @IBOutlet weak var tvCatatan: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initTextViewStyle()
    }
    
    private func initTextViewStyle(){
        tvCatatan.setGeneralStyle()
    }
}
