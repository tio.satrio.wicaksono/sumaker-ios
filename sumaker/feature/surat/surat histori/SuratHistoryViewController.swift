//
//  SuratHistoryViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 13/07/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class SuratHistoryViewController: BaseViewController<SuratHistoryPresenter>, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tblSuratHistory: UITableView!
    var listSuratHistory = DummyUtil.initDummySuratHistoryModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDelegate()
        initDataSource()
    }
    
    private func initDelegate(){
        tblSuratHistory.delegate = self
    }
    
    private func initDataSource(){
        tblSuratHistory.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSuratHistory.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = listSuratHistory[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuratHistoryViewCell", for: indexPath) as! SuratHistoryViewCell
        cell.ivSuratHistory.image = UIImage(named: model.image)
        cell.lblKepada.text = "Kepada: \(model.kepada)"
        cell.lblNama.text = model.nama
        cell.lblWaktu.text = model.waktu
        return cell
    }
    
}
