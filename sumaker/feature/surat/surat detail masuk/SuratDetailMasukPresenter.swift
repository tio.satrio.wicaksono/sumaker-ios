//
//  SuratDetailPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 24/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class SuratDetailMasukPresenter: BasePresenter<SuratDetailMasukDelegate>{
    func inboxRead(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestReadInbox(paramInput: paramInput, completion: { model in
            self.view.didSuccessLoad(inboxReadModel: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
}

protocol SuratDetailMasukDelegate: BaseDelegate {
    func didSuccessLoad(inboxReadModel: InboxOutboxReadModel)
}
