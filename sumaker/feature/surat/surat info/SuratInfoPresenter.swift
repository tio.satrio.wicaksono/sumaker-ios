//
//  SuratInfoPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 14/08/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class SuratInfoPresenter: BasePresenter<SuratInfoDelegate>{
    func requestListSuratInfoHistory(paramInput: ParamInput){
        self.view.taskDidBegin()
        Api.requestGetSuratInfoHistory(paramInput: paramInput, completion: { (models) in
            self.view.didLoadListSuratInfo(list: models)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidError(message: err.localizedDescription)
            self.view.taskDidFinish()
        }
    }
}

protocol SuratInfoDelegate: BaseDelegate {
    func didLoadListSuratInfo(list: [SuratInfoHistoryModel])
}
