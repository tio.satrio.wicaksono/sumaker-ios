//
//  SuratInfoViewCell.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 14/08/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class SuratInfoViewCell: UITableViewCell{
    
    @IBOutlet weak var lblNamaKe: UILabel!
    @IBOutlet weak var lblNamaDari: UILabel!
    @IBOutlet weak var ivTtd: UIImageView!
    @IBOutlet weak var lblTglBaca: UILabel!
    @IBOutlet weak var lblKe: UILabel!
    @IBOutlet weak var lblDari: UILabel!
    @IBOutlet weak var lblTglKirim: UILabel!
}
