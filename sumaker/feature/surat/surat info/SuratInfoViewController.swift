//
//  SuratInfoViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 14/08/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class SuratInfoViewController: UITableViewController, SuratInfoDelegate{
   
    
    var listHistoryInfo : [SuratInfoHistoryModel] = []
    var presenter: SuratInfoPresenter!
    var mailId: String!
    let paramInput = ParamInput()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initParamInput()
        initPresenter()
        presenter.requestListSuratInfoHistory(paramInput: paramInput)
    }
    
    private func initPresenter(){
        presenter = SuratInfoPresenter(view: self)
    }
    
    private func initParamInput(){
        paramInput.mailId = mailId
    }
    
    private func processCell(cell: SuratInfoViewCell, model: SuratInfoHistoryModel) -> SuratInfoViewCell{
        cell.ivTtd.kf.setImage(with: URL(string: model.paraf), placeholder: UIImage(named: "PlaceholderIcon"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.lblDari.text = "\(model.jabatanDari) (\(model.unitKerjaDari))"
        cell.lblNamaDari.text = model.pegawaiDari
        cell.lblNamaKe.text = model.pegawaiKe
        cell.lblKe.text =  "\(model.jabatanKe) (\(model.unitKerjaKe))"
        cell.lblTglKirim.text = model.tglKirim
        
        if let tglBaca = model.tglBaca {
            cell.lblTglBaca.text = tglBaca
        } else {
            cell.lblTglBaca.text = "-"
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listHistoryInfo.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = listHistoryInfo[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuratInfoViewCell", for: indexPath) as! SuratInfoViewCell
        
        return processCell(cell: cell, model: model)
    }
    
    func taskDidBegin() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func taskDidFinish() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
    func didLoadListSuratInfo(list: [SuratInfoHistoryModel]) {
        var currentIndex = listHistoryInfo.count
        tableView.beginUpdates()
        list.forEach { (surat) in
            listHistoryInfo.insert(surat, at: currentIndex)
            tableView.insertRows(at: [IndexPath(row: currentIndex, section: 0)], with: .automatic)
            currentIndex = currentIndex + 1
        }
        tableView.endUpdates()
    }
}
