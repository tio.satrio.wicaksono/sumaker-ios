//
//  DistribusiSuratPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 09/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class DistribusiSuratMasukPresenter: BasePresenter<DistribusiSuratMasukDelegate>{
    func sendDistribusiSuratMasuk(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestSendDistribusiSuratMasuk(paramInput: paramInput, completion: { model in
            self.view.didSuccessSendDistribusiSuratMasuk()
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
}

protocol DistribusiSuratMasukDelegate: BaseDelegate {
    func didSuccessSendDistribusiSuratMasuk()
}
