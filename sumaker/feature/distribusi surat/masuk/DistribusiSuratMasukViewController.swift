//
//  DistribusiSuratViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 09/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit
import FileBrowser

class DistribusiSuratMasukViewController: BaseViewController<DistribusiSuratMasukPresenter>,
UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, 
RetrieveSelectedDelegate, SifatSuratDelegate, DistribusiSuratMasukDelegate{
    
    @IBOutlet weak var inputNoSurat: UITextField!
    @IBOutlet weak var inputSuratDari: UITextField!
    @IBOutlet weak var inputDitujukanKepada: UITextField!
    @IBOutlet weak var inputTanggalSurat: UITextField!
    @IBOutlet weak var inputSifatSurat: UITextField!
    @IBOutlet weak var inputPerihal: UITextField!
    @IBOutlet weak var inputRingkasanSurat: UITextView!
    @IBOutlet weak var inputTanggalMulai: UITextField!
    @IBOutlet weak var inputTanggalAkhir: UITextField!
    @IBOutlet weak var inputLokasi: UITextView!
    @IBOutlet weak var lblFileSuratName: UILabel!
    @IBOutlet weak var lblFileLampiranName: UILabel!
    
    let datePickerSurat = UIDatePicker()
    let dateTimePickerAwal = UIDatePicker()
    let dateTimePickerAkhir = UIDatePicker()
    let pvSifatSurat = UIPickerView()
    let paramInput = ParamInput()
    var listDitujukanKepada = Array<ChildrenDitujukanKepada>()
    var listSifatSurat = Array<SuratSifatDistribusiModel>()
    var sifatSuratPresenter : SifatSuratPresenter!
    var fileSurat: FBFile?
    var fileLampiran: FBFile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDatePicker()
        initDelegate()
        initInputView()
        initTextViewStyle()
        initParamInput()
        initPresenter()
    }
    
    @IBAction func btnFileSuratTapped(_ sender: Any) {
        let fileBrowser = FileBrowser()
        fileBrowser.didSelectFile = { (file) in
            if file.fileExtension == "docx" || file.fileExtension == "doc" {
                self.lblFileSuratName.text = file.displayName
                self.lblFileSuratName.isHidden = false
                self.fileSurat = file
            } else {
                self.showError(title: "Pesan", message: "File ekstensi harus .docx / .doc")
                self.lblFileSuratName.isHidden = true
                self.fileSurat = nil
            }
        }
        present(fileBrowser, animated: true, completion: nil)
    }
    
    @IBAction func btnFileLampiranTapped(_ sender: Any) {
        let fileBrowser = FileBrowser()
        fileBrowser.didSelectFile = { (file) in
            if file.fileExtension == "docx"
                || file.fileExtension == "doc"
                || file.fileExtension == "pdf"
                || file.fileExtension == "xls"
                || file.fileExtension == "xlsx"
                || file.fileExtension == "zip"
            {
                self.lblFileLampiranName.text = file.displayName
                self.lblFileLampiranName.isHidden = false
                self.fileLampiran = file
            } else {
                self.showError(title: "Pesan", message: "File ekstensi harus .docx /.doc / .pdf / .xls / .xlsx / .zip")
                self.lblFileLampiranName.isHidden = true
                self.fileLampiran = nil
            }
        }
        present(fileBrowser, animated: true, completion: nil)
    }
    
    @IBAction func parafTapped(_ sender: Any) {
        inputProcess()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let viewController = getViewController(storyboardName: "Main", identifier: "DitujukanKepadaViewController") as! DitujukanKepadaViewController
        let navController = UINavigationController(rootViewController: viewController)
        viewController.retrieveSelectedDelegate = self
        listDitujukanKepada.removeAll()
        inputDitujukanKepada.text = ""
        present(navController, animated: true, completion: nil)
        return true
    }

    func onRetrieveSelectedData(list: [ChildrenDitujukanKepada]) {
        var selectedValue = ""
        list.forEach { (selectedChild) in
            selectedValue = selectedValue + ";" + selectedChild.text
        }
        if selectedValue.count > 0 {
            selectedValue.removeFirst()
            selectedValue.removeLast()
        }
        listDitujukanKepada = list
        inputDitujukanKepada.text = selectedValue
    }
    
    func didLoadSifatSuratDistribusi(list: Array<SuratSifatDistribusiModel>) {
        listSifatSurat = list
        pvSifatSurat.reloadAllComponents()
    }
    
    func taskDidBegin() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func taskDidFinish() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func didSuccessSendDistribusiSuratMasuk() {
        showError(title: "Pesan", message: "Berhasil kirim distribusi surat masuk", positiveText: "OK") { (UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listSifatSurat.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listSifatSurat[row].label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        inputSifatSurat.text = listSifatSurat[row].label
    }
    
    private func initDelegate() {
        inputDitujukanKepada.delegate = self
        pvSifatSurat.delegate = self
    }
    
    private func initInputView() {
        inputDitujukanKepada.inputView = UIView()
        inputTanggalSurat.inputView = datePickerSurat
        inputTanggalMulai.inputView = dateTimePickerAwal
        inputTanggalAkhir.inputView = dateTimePickerAkhir
        inputSifatSurat.inputView = pvSifatSurat
    }
    
    private func initDatePicker() {
        datePickerSurat.datePickerMode = .date
        datePickerSurat.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        dateTimePickerAwal.datePickerMode = .dateAndTime
        dateTimePickerAwal.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        dateTimePickerAkhir.datePickerMode = .dateAndTime
        dateTimePickerAkhir.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
    }
    
    private func initDataSource() {
        pvSifatSurat.dataSource = self
    }
    
    private func initTextViewStyle(){
        inputRingkasanSurat.setGeneralStyle()
        inputLokasi.setGeneralStyle()
    }
    
    private func initParamInput(){
        guard let currentUser = DBUtil.getInstance().getCurrentUser() else {
            return
        }
        paramInput.userId = currentUser.userId!
    }
    
    private func initPresenter(){
        presenter = DistribusiSuratMasukPresenter(view: self)
        sifatSuratPresenter = SifatSuratPresenter(view: self)
        sifatSuratPresenter.getSifatSuratDistribusi(paramInput: paramInput)
    }
    
    private func inputProcess(){
        let validator = ValidatorUtil()
        do{
            let userIdKonseptor = DBUtil.getInstance().getCurrentUser()!.userId!
            let ditujukanKepada = processDitujukanKepada()
            let noSurat = inputNoSurat.text!
            let sifatSurat = Int(listSifatSurat[pvSifatSurat.selectedRow(inComponent: 0)].id)
            let perihal = inputPerihal.text!
            let dariSurat = inputSuratDari.text!
            let ringkasanSurat = inputRingkasanSurat.text!
            let tanggalSurat = inputTanggalSurat.text!
            
            paramInput.userIdKonseptor = userIdKonseptor
            paramInput.idPegawaiTujuans = ditujukanKepada
            paramInput.noSurat = noSurat
            paramInput.tglSurat = tanggalSurat
            paramInput.sifatSurat = sifatSurat!
            paramInput.perihal = perihal
            paramInput.dariSurat = dariSurat
            paramInput.isiSurat = ringkasanSurat
            paramInput.userFile = try validator.validateFile(file: fileSurat)
            paramInput.fileLampiran = fileLampiran
            presenter.sendDistribusiSuratMasuk(paramInput: paramInput)
            
        }catch ValidatorError.error(let reason){
            showError(title: "Pesan", message: reason)
        }catch{
            showError(title: "Pesan", message: error.localizedDescription)
        }
    }
    
    private func processDitujukanKepada() -> String{
        var ditujukanKepada = ""
        if listDitujukanKepada.count > 0{
            listDitujukanKepada.forEach { (model) in
                ditujukanKepada = ditujukanKepada + model.id + ","
            }
            ditujukanKepada.removeLast()
        }
        return ditujukanKepada
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        if sender == datePickerSurat {
            inputTanggalSurat.text = DatePickerUtil.formatDate(date: sender.date)
        } else if sender == dateTimePickerAwal {
            inputTanggalMulai.text = DatePickerUtil.formatDateTime(date: sender.date)
        } else if sender == dateTimePickerAkhir {
            inputTanggalAkhir.text = DatePickerUtil.formatDateTime(date: sender.date)
        }
    }
}
