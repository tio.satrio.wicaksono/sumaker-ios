//
//  ParentCell.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 11/09/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class ParentViewCell: UITableViewHeaderFooterView {
    typealias Action = (ParentViewCell, Int) -> Void
    typealias ActionCheck = (Int) -> Void
    var rowSection: Int?
    var tapped: Action?
    var checked: ActionCheck?
    var isChecked: Bool?
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var arrowIcon: UIImageView!
    @IBOutlet weak var checkboxIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        rowSection = 0
        isChecked = false
        checkboxIcon.isHidden = true
        checkboxIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onChecked)))
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapped)))
    }
    
    @objc func onTapped(gesture: UITapGestureRecognizer){
        tapped?(self, rowSection!)
    }
    
    @objc func onChecked(gesture: UITapGestureRecognizer){
        checked?(rowSection!)
    }
}
