//
//  DitujukanKepadaViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 14/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit
import DeepDiff

class DitujukanKepadaViewController: UITableViewController, DitujukanKepadaDelegate{
    
    var list = Array<PickerViewModel>()
    var listCurrent = Array<DitujukanKepadaModel>()
    var ditujukanKepada : DitujukanKepadaModel1!
    var selectedDitujuKepada : [ChildrenDitujukanKepada] = []
    
    var retrieveSelectedDelegate: RetrieveSelectedDelegate?
    var presenter: DitujukanKepadaPresenter!
    let param: ParamInput = ParamInput()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: "DitujukanKepadaView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ParentViewCell")
        param.userId = DBUtil.getInstance().getCurrentUser()!.userId!
        presenter.requestGetDitujukanKepada(paramInput: param)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if ditujukanKepada.tree[section].state == "closed"{
            return 0
        } else {
            return ditujukanKepada.tree[section].children.count
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if ditujukanKepada == nil {
            return 0
        } else {
            return ditujukanKepada.tree.count
        }
    }
    
    private func initPresenter(){
        presenter = DitujukanKepadaPresenter(view: self)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let model = ditujukanKepada.tree[indexPath.section].children[indexPath.row]
        if selectedDitujuKepada.contains(where: { $0.id == model.id }) {
           cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.textLabel?.text = ditujukanKepada.tree[indexPath.section].children[indexPath.row].text
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! ParentViewCell
        if self.ditujukanKepada.tree[section].state == "open" {
            header.arrowIcon.isHidden = true
            header.checkboxIcon.isUserInteractionEnabled = true
            header.checkboxIcon.isHidden = false
            header.rowSection = section
            header.checked = {(section) in
                if header.isChecked! {
                    header.isChecked = false
                    header.checkboxIcon.image = UIImage(named: "UncheckboxIcon")
                    let parent = self.ditujukanKepada.tree[section]
                    if let index = self.selectedDitujuKepada.index(where: { (child) -> Bool in
                        child.parent == parent.id
                    }) {
                        self.selectedDitujuKepada.remove(at: index)
                    }
                    let listIndextPath = tableView.indexPathsForRowsInSection(section)
                    listIndextPath.forEach({ (indexPath) in
                        let cell = tableView.cellForRow(at: indexPath)
                        cell?.accessoryType = .none
                    })
                } else {
                    header.isChecked = true
                    header.checkboxIcon.image = UIImage(named: "CheckboxIcon")
                    let listIndexPath = tableView.indexPathsForRowsInSection(section)
                    listIndexPath.forEach({ (indexPath) in
                        let cell = tableView.cellForRow(at: indexPath)
                        let model = self.ditujukanKepada.tree[indexPath.section].children[indexPath.row]
                        cell?.accessoryType = .checkmark
                        self.selectedDitujuKepada.append(model)
                    })
                    
                }
            }
        } else {
            header.arrowIcon.isHidden = false
            header.checkboxIcon.isUserInteractionEnabled = false
            header.checkboxIcon.isHidden = true
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ParentViewCell") as! ParentViewCell
        headerCell.lblTitle.text = ditujukanKepada.tree[section].text
        headerCell.rowSection = section
        headerCell.tapped = { (cell, rowSection) in
            if self.ditujukanKepada.tree[rowSection].state == "open" {
                self.ditujukanKepada.tree[rowSection].state = "closed"
            } else {
                self.ditujukanKepada.tree[rowSection].state = "open"
            }
            tableView.reloadSections(IndexSet(integer: rowSection), with: .automatic)
        }
        return headerCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let header = tableView.headerView(forSection: indexPath.section) as! ParentViewCell
        let cell = tableView.cellForRow(at: indexPath)
        let childDitujukanKepada = ditujukanKepada.tree[indexPath.section].children[indexPath.row]
        let listIndexPath = tableView.indexPathsForRowsInSection(indexPath.section)
        if (cell?.accessoryType == UITableViewCellAccessoryType.none) {
            var isAllSelected: Bool = true
            cell?.accessoryType = .checkmark
            selectedDitujuKepada.append(childDitujukanKepada)
            listIndexPath.forEach { (indexPathInSection) in
                let cellInSection = tableView.cellForRow(at: indexPathInSection)
                if cellInSection?.accessoryType == UITableViewCellAccessoryType.none {
                    isAllSelected = false
                    return
                }
            }
            if isAllSelected {
                header.isChecked = true
                header.checkboxIcon.image = UIImage(named: "CheckboxIcon")
            }
            
        } else {
            cell?.accessoryType = .none
            var removeIndex = -1
            for i in 0 ..< selectedDitujuKepada.count {
                if selectedDitujuKepada[i].id == childDitujukanKepada.id {
                    removeIndex = i
                    break
                }
            }
            if removeIndex != -1 {
                selectedDitujuKepada.remove(at: removeIndex)
                header.isChecked = false
                header.checkboxIcon.image = UIImage(named: "UncheckboxIcon")
            }
        }
    }
    
    func taskDidBegin() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didLoadDitujukanKepada(model: DitujukanKepadaModel1) {
        ditujukanKepada = model
        for i in 0 ..< ditujukanKepada.tree.count {
            ditujukanKepada.tree[i].state = "closed"
        }
        tableView.reloadData()
    }
    
    func taskDidFinish() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
    @IBAction func tappedDone(_ sender: Any) {

        dismiss(animated: true) {
            self.retrieveSelectedDelegate?.onRetrieveSelectedData(list: self.selectedDitujuKepada)
        }
    }
    
    @IBAction func tappedCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

protocol RetrieveSelectedDelegate {
    func onRetrieveSelectedData(list: [ChildrenDitujukanKepada])
}
