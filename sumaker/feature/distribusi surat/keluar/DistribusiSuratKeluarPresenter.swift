//
//  DistribusiSuratKeluarPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 21/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class DistribusiSuratKeluarPresenter: BasePresenter<DistribusiSuratKeluarDelegate>{
    func sendDistribusiSuratKeluar(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestSendDistribusiSuratMasuk(paramInput: paramInput, completion: { model in
            self.view.didSuccessSendDistribusiSuratKeluar()
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
}

protocol DistribusiSuratKeluarDelegate: BaseDelegate {
    func didSuccessSendDistribusiSuratKeluar()
}
