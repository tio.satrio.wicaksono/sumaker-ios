//
//  DistribusiSuratKeluarViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 21/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit
import FileBrowser

class DistribusiSuratKeluarViewController: BaseViewController<DistribusiSuratKeluarPresenter>, UITextFieldDelegate, RetrieveSelectedDelegate, UIPickerViewDelegate, SifatSuratDelegate,
UIPickerViewDataSource, PenandatanganDelegate, DistribusiSuratKeluarDelegate{
    
    @IBOutlet weak var inputAkanDiTtdOleh: UITextField!
    @IBOutlet weak var inputDitujukanKepada: UITextField!
    @IBOutlet weak var inputAkanDiperiksaOleh: UITextField!
    @IBOutlet weak var inputTanggalSurat: UITextField!
    @IBOutlet weak var inputSifatSurat: UITextField!
    @IBOutlet weak var inputPerihal: UITextField!
    @IBOutlet weak var inputRingkasanSurat: UITextView!
    @IBOutlet weak var inputTanggalMulai: UITextField!
    @IBOutlet weak var inputTanggalAkhir: UITextField!
    @IBOutlet weak var inputLokasi: UITextView!
    @IBOutlet weak var lblFileSuratName: UILabel!
    @IBOutlet weak var lblFileLampiranName: UILabel!
    
    let datePickerSurat = UIDatePicker()
    let dateTimePickerAwal = UIDatePicker()
    let dateTimePickerAkhir = UIDatePicker()
    let pvSifatSurat = UIPickerView()
    let pvPenandatangan = UIPickerView()
    var listAkanDiperiksaOleh = Array<ChildrenDitujukanKepada>()
    var listSifatSurat = Array<SuratSifatDistribusiModel>()
    var listPenandatangan = Array<PenandatanganModel>()
    var penandatanganPresenter : PenandatanganPresenter!
    var sifatSuratPresenter : SifatSuratPresenter!
    var paramInput : ParamInput!
    var fileSurat: FBFile?
    var fileLampiran: FBFile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDatePicker()
        initDelegate()
        initInputView()
        initTextViewStyle()
        initPresenter()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let viewController = getViewController(storyboardName: "Main", identifier: "DitujukanKepadaViewController") as! DitujukanKepadaViewController
        let navController = UINavigationController(rootViewController: viewController)
        viewController.retrieveSelectedDelegate = self
        listAkanDiperiksaOleh.removeAll()
        inputAkanDiperiksaOleh.text = ""
        present(navController, animated: true, completion: nil)
        return true
    }

    func onRetrieveSelectedData(list: [ChildrenDitujukanKepada]) {
        var selectedValue = ""
        list.forEach { (selectedChild) in
            selectedValue = selectedValue + ";" + selectedChild.text
        }
        if selectedValue.count > 0 {
            selectedValue.removeFirst()
            selectedValue.removeLast()
        }
        listAkanDiperiksaOleh = list
        inputAkanDiperiksaOleh.text = selectedValue
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case pvSifatSurat:
            return listSifatSurat.count
        case pvPenandatangan:
            return listPenandatangan.count
        default:
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case pvSifatSurat:
            return listSifatSurat[row].label
        case pvPenandatangan:
            return listPenandatangan[row].namaPegawai
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView{
        case pvSifatSurat:
            inputSifatSurat.text = listSifatSurat[row].label
        case pvPenandatangan:
            let ttdOleh = "\(listPenandatangan[row].namaPegawai) (\(listPenandatangan[row].jabatan) - \(listPenandatangan[row].unitKerja))"
            inputAkanDiTtdOleh.text = ttdOleh
        default:
            inputAkanDiTtdOleh.text = ""
            inputSifatSurat.text = ""
        }
    }
    
    private func initDelegate() {
        inputAkanDiperiksaOleh.delegate = self
        pvSifatSurat.delegate = self
        pvPenandatangan.delegate = self
    }
    
    private func initInputView() {
        inputAkanDiperiksaOleh.inputView = UIView()
        inputTanggalSurat.inputView = datePickerSurat
        inputTanggalMulai.inputView = dateTimePickerAwal
        inputTanggalAkhir.inputView = dateTimePickerAkhir
        inputSifatSurat.inputView = pvSifatSurat
        inputAkanDiTtdOleh.inputView = pvPenandatangan
    }
    
    private func initDatePicker() {
        datePickerSurat.datePickerMode = .date
        datePickerSurat.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        dateTimePickerAwal.datePickerMode = .dateAndTime
        dateTimePickerAwal.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        dateTimePickerAkhir.datePickerMode = .dateAndTime
        dateTimePickerAkhir.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
    }
    
    private func initDataSource() {
        pvSifatSurat.dataSource = self
        pvPenandatangan.dataSource = self
    }
    
    private func initTextViewStyle(){
        inputRingkasanSurat.setGeneralStyle()
        inputLokasi.setGeneralStyle()
    }
    
    private func initPresenter(){
        initParamInput()
        presenter = DistribusiSuratKeluarPresenter(view: self)
        penandatanganPresenter = PenandatanganPresenter(view: self)
        penandatanganPresenter.requestGetPenandatangan(paramInput: paramInput)
        sifatSuratPresenter = SifatSuratPresenter(view: self)
        sifatSuratPresenter.getSifatSuratDistribusi(paramInput: paramInput)
    }
    
    private func initParamInput(){
        paramInput = ParamInput()
        guard let currentUser = DBUtil.getInstance().getCurrentUser() else {
            return
        }
        paramInput.userId = currentUser.userId!
    }
    
    private func processDiperiksaOleh() -> String{
        var diperiksaOleh = ""
        if listAkanDiperiksaOleh.count > 0{
            listAkanDiperiksaOleh.forEach { (model) in
                diperiksaOleh = diperiksaOleh + model.id + ","
            }
            diperiksaOleh.removeLast()
        }
        return diperiksaOleh
    }

    
    private func inputProcess(){
        let validator = ValidatorUtil()
        do{
            let userIdKonseptor = DBUtil.getInstance().getCurrentUser()!.userId!
            try validator.validateText(text: inputAkanDiTtdOleh.text!, type: nil, name: "Akan di TTD Oleh")
            let ttdOleh = Int(listPenandatangan[pvPenandatangan.selectedRow(inComponent: 0)].id)
            let ditujukanKepada = try validator.validateText(text: inputDitujukanKepada.text!, type: nil, name: "Ditujukan Kepada")
            let diperiksaOleh = try validator.validateText(text: processDiperiksaOleh(), type: nil, name: "Akan Diperiksa/Diparaf Oleh")
            let tanggalSurat = inputTanggalSurat.text!
            try validator.validateText(text: inputSifatSurat.text!, type: nil, name: "Sifat Surat")
            let sifatSurat = Int(listSifatSurat[pvSifatSurat.selectedRow(inComponent: 0)].id)
            let perihal = inputPerihal.text!
            let ringkasanSurat = inputRingkasanSurat.text!
            
            paramInput.userIdKonseptor = userIdKonseptor
            paramInput.userIdTtd = (ttdOleh?.description)!
            paramInput.kepadaSurat = ditujukanKepada
            paramInput.idPegawaiTujuans = diperiksaOleh
            paramInput.tglSurat = tanggalSurat
            paramInput.sifatSurat = sifatSurat!
            paramInput.perihal = perihal
            paramInput.isiSurat = ringkasanSurat
            paramInput.userFile = try validator.validateFile(file: fileSurat)
            paramInput.fileLampiran = fileLampiran
            presenter.sendDistribusiSuratKeluar(paramInput: paramInput)
            
        }catch ValidatorError.error(let reason){
            showError(title: "Pesan", message: reason)
        }catch{
            showError(title: "Pesan", message: error.localizedDescription)
        }
    }
    
    func taskDidBegin() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didLoadPenandatangan(models: [PenandatanganModel]) {
        listPenandatangan = models
    }
    
    func didSuccessSendDistribusiSuratKeluar() {
        showError(title: "Pesan", message: "Berhasil kirim distribusi surat keluar", positiveText: "OK") { (UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didLoadSifatSuratDistribusi(list: Array<SuratSifatDistribusiModel>) {
        listSifatSurat = list
        pvSifatSurat.reloadAllComponents()
    }
    
    func taskDidFinish() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        if sender == datePickerSurat {
            inputTanggalSurat.text = DatePickerUtil.formatDate(date: sender.date)
        } else if sender == dateTimePickerAwal {
            inputTanggalMulai.text = DatePickerUtil.formatDateTime(date: sender.date)
        } else if sender == dateTimePickerAkhir {
            inputTanggalAkhir.text = DatePickerUtil.formatDateTime(date: sender.date)
        }
    }
    
    @IBAction func btnFileSuratTapped(_ sender: Any) {
        let fileBrowser = FileBrowser()
        fileBrowser.didSelectFile = { (file) in
            if file.fileExtension == "docx" || file.fileExtension == "doc" {
                self.lblFileSuratName.text = file.displayName
                self.lblFileSuratName.isHidden = false
                self.fileSurat = file
            } else {
                self.showError(title: "Pesan", message: "File ekstensi harus .docx / .doc")
                self.lblFileSuratName.isHidden = true
                self.fileSurat = nil
            }
        }
        present(fileBrowser, animated: true, completion: nil)
    }
    
    @IBAction func btnFileLampiranTapped(_ sender: Any) {
        let fileBrowser = FileBrowser()
        fileBrowser.didSelectFile = { (file) in
            if file.fileExtension == "docx"
                || file.fileExtension == "doc"
                || file.fileExtension == "pdf"
                || file.fileExtension == "xls"
                || file.fileExtension == "xlsx"
                || file.fileExtension == "zip"
            {
                self.lblFileLampiranName.text = file.displayName
                self.lblFileLampiranName.isHidden = false
                self.fileLampiran = file
            } else {
                self.showError(title: "Pesan", message: "File ekstensi harus .docx /.doc / .pdf / .xls / .xlsx / .zip")
                self.lblFileLampiranName.isHidden = true
                self.fileLampiran = nil
            }
        }
        present(fileBrowser, animated: true, completion: nil)
    }
    
    @IBAction func onTappedParaf(_ sender: Any) {
        inputProcess()
    }
}
