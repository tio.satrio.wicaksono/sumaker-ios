//
//  PenandatanganPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 18/12/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class PenandatanganPresenter: BasePresenter<PenandatanganDelegate>{
    func requestGetPenandatangan(paramInput: ParamInput){
        self.view.taskDidBegin()
        Api.requestPenandatangan(paramInput: paramInput, completion: { (models) in
            self.view.didLoadPenandatangan(models: models)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidError(message: err.localizedDescription)
            self.view.taskDidFinish()
        }
    }
}

protocol PenandatanganDelegate: BaseDelegate{
    func didLoadPenandatangan(models: [PenandatanganModel])
}
