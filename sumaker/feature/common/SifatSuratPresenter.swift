//
//  SifatSuratPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 28/08/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class SifatSuratPresenter: BasePresenter<SifatSuratDelegate>{
    
    func getSifatSurat(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestGetSifatSurat(paramInput: paramInput, completion: { (model) in
            self.view.didLoadSifatSurat(list: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
    
    func getSifatSuratDistribusi(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestGetSifatSuratDistribusi(paramInput: paramInput, completion: { (model) in
            self.view.didLoadSifatSuratDistribusi(list: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
    
}

protocol SifatSuratDelegate: BaseDelegate {
    func didLoadSifatSurat(list: Array<SifatSuratModel>)
    func didLoadSifatSuratDistribusi(list: Array<SuratSifatDistribusiModel>)
}

extension SifatSuratDelegate{
    func didLoadSifatSurat(list: Array<SifatSuratModel>){}
    func didLoadSifatSuratDistribusi(list: Array<SuratSifatDistribusiModel>){}
}
