//
//  DitujukanKepada.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 11/09/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class DitujukanKepadaPresenter: BasePresenter<DitujukanKepadaDelegate>{
    func requestGetDitujukanKepada(paramInput: ParamInput){
        self.view.taskDidBegin()
        Api.requestGetDitujukanKepada(paramInput: paramInput, completion: { (models) in
            self.view.didLoadDitujukanKepada(model: models)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidError(message: err.localizedDescription)
            self.view.taskDidFinish()
        }
    }
}

protocol DitujukanKepadaDelegate: BaseDelegate {
    func didLoadDitujukanKepada(model: DitujukanKepadaModel1)
}
