//
//  InboxTabPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 13/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class InboxTabPresenter: BasePresenter<InboxTabDelegate>{
    func getInboxTab(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestGetInboxTab(paramInput: paramInput, completion: { model in
            self.view.didLoadInboxTab(list: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
}

protocol InboxTabDelegate: BaseDelegate {
    func didLoadInboxTab(list: Array<InboxTabModel>)
}
