//
//  MainPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 09/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class MainPresenter: BasePresenter<MainDelegate>{
    func getTotalUnread(paramInput: ParamInput){
        self.view.taskDidBegin()
        Api.requestGetTotalUnread(paramInput: paramInput, completion: { (model) in
            self.view.didLoadUnread(model: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidError(message: err.localizedDescription)
            self.view.taskDidFinish()
        }
    }
}

protocol MainDelegate: BaseDelegate {
    func didLoadUnread(model: TotalUnreadModel)
}
