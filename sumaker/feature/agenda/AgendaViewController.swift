//
//  AgendaViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 23/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar

class AgendaViewController: BaseViewController<AgendaPresenter>,
FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance,
AgendaDelegate, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calendar: FSCalendar!
    let paramInput = ParamInput()
    var dateAgendas = [[Date]]()
    var agendaModels = [AgendaModel]()
    var agenda : AgendaModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCalendar()
        initPresenter()
        initTableView()
    }
    
    private func initCalendar(){
        calendar.allowsMultipleSelection = true
        calendar.delegate = self
        calendar.dataSource = self
        
    }
    
    private func initTableView(){
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func initPresenter(){
        initParamInput()
        presenter = AgendaPresenter(view: self)
        let currMonth = calendar.month(of: calendar.currentPage).description
        let currYear = calendar.year(of: calendar.currentPage).description
        getAgenda(bulan: currMonth, tahun: currYear)
    }
    
    private func initParamInput(){
        guard let currentUser = DBUtil.getInstance().getCurrentUser() else {
            return
        }
        paramInput.userId = currentUser.userId!
    }
    
    private func getAgenda(bulan: String, tahun: String){
        paramInput.bulan = bulan
        paramInput.tahun = tahun
        presenter.agenda(paramInput: paramInput)
    }
    
    func taskDidBegin() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didLoadSuccessAgenda(models: [AgendaModel]) {
        dump(models)
        agendaModels = models
        dateAgendas.removeAll()
        models.forEach { (agenda) in
            let awal = agenda.awalKegiatan.parseDateTimeToDate()
            let akhir = agenda.akhirKegiatan.parseDateTimeToDate()
            let dateRange = Date.dates(from: awal.parseToDate(), to: akhir.parseToDate())
            dateRange.forEach({ (date) in
                calendar.select(date)
            })
            dateAgendas.append(dateRange)
        }
        calendar.reloadData()
        
    }
    
    func taskDidFinish() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if agenda != nil {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if agenda != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
            cell?.textLabel!.text = agenda?.prihal
            cell?.detailTextLabel!.text = agenda?.tempatKegiatan
            return cell!
        } else {
            return UITableViewCell()
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if calendar.selectedDate != date {
            calendar.select(date)
        }
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if agendaModels.count == 0 {
            self.agenda = nil
            tableView.reloadData()
        }
        agendaModels.forEach { (agenda) in
            let awal = agenda.awalKegiatan.parseDateTimeToDate()
            let akhir = agenda.akhirKegiatan.parseDateTimeToDate()
            let dateRange = Date.dates(from: awal.parseToDate(), to: akhir.parseToDate())
            if dateRange.contains(date){
                self.agenda = agenda
                calendar.select(date)
                tableView.reloadData()
                return
            } else {
                self.agenda = nil
                tableView.reloadData()
            }
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {

        if date.parseToStringWithFormat() == Date().parseToStringWithFormat() {
            return UIColor.green
        } else {
            return UIColor.blue
        }
    }

    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let monthChanged = calendar.month(of: calendar.currentPage).description
        let yearChanged = calendar.year(of: calendar.currentPage).description
        getAgenda(bulan: monthChanged, tahun: yearChanged)
    }
    
}
