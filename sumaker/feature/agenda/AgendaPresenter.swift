//
//  AgendaPresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 23/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class AgendaPresenter: BasePresenter<AgendaDelegate>{
    func agenda(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestAgenda(paramInput: paramInput, completion: { model in
            self.view.didLoadSuccessAgenda(models: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
}

protocol AgendaDelegate: BaseDelegate {
    func didLoadSuccessAgenda(models: [AgendaModel])
}
