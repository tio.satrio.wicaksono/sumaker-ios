//
//  MainViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 30/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class MainViewController: BaseViewController<MainPresenter>,
iCarouselDataSource, iCarouselDelegate, MainDelegate {
    
    @IBOutlet weak var carouselView: iCarousel!
    
    var listMenu = Array<CarouselMenuModel>()
    let paramInput = ParamInput()
    var totalUnread : TotalUnreadModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listMenu = CarouselMenuUtil.initDataModel()
        carouselView.dataSource = self
        carouselView.delegate = self
        carouselView.type = .cylinder
        let userIdentity = DBUtil.getInstance().getCurrentUser()!
        print("TOKEN : \(userIdentity.token!)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initPresenter()
    }
    
    private func initPresenter(){
        guard let userId = DBUtil.getInstance().getCurrentUser()?.userId else {
            return
        }
        paramInput.userId = userId
        presenter = MainPresenter(view: self)
        presenter.getTotalUnread(paramInput: paramInput)
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return listMenu.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let customView = Bundle.main.loadNibNamed("CarouselReuseView", owner: self, options: nil)?.first as! CarouselReuseView
        customView.index = index
        customView.ivCarousel.image = listMenu[index].ivMenu
        customView.lblCarousel.text = listMenu[index].textMenu
        if index == 0 {
            if totalUnread != nil {
                customView.lblBadge.text = totalUnread.jumlah
                customView.backgroundBadge.isHidden = false
                customView.lblBadge.isHidden = false
            }
        }
        customView.tapped = {(customView, index) in
            self.selectMenu(index: index)
        }
        return customView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 2.0
        }
        return value
    }
    
    func taskDidBegin() {
        isFullscreenActivityIndicatorVisible = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didLoadUnread(model: TotalUnreadModel) {
        totalUnread = model
        carouselView.reloadData()
    }
    
    func taskDidFinish() {
        isFullscreenActivityIndicatorVisible = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
    
    private func selectMenu(index: Int){
        switch index {
        case 0:
            let viewController = getViewController(storyboardName: "Main", identifier: "SuratMasukViewController") as! SuratMasukViewController
            navigationController?.pushViewController(viewController, animated: true)
        case 1:
            let viewController = getViewController(storyboardName: "Main", identifier: "DistribusiSuratMasukViewController") as! DistribusiSuratMasukViewController
            navigationController?.pushViewController(viewController, animated: true)
        case 2:
            let viewController = getViewController(storyboardName: "Main", identifier: "DistribusiSuratKeluarViewController") as! DistribusiSuratKeluarViewController
            navigationController?.pushViewController(viewController, animated: true)
        case 3:
            let viewController = getViewController(storyboardName: "Main", identifier: "SuratTerkirimViewController") as! SuratTerkirimViewController
            navigationController?.pushViewController(viewController, animated: true)
        case 4:
            let viewController = getViewController(storyboardName: "Main", identifier: "ProfileViewController") as! ProfileViewController
            navigationController?.pushViewController(viewController, animated: true)
        case 5:
            let viewController = getViewController(storyboardName: "Main", identifier: "AgendaViewController") as! AgendaViewController
            navigationController?.pushViewController(viewController, animated: true)
        default:
            print("\(index)")
        }
    }
}
