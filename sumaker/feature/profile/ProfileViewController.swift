//
//  ProfileViewController.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 12/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class ProfileViewController: BaseViewController<ProfilePresenter>, UITableViewDelegate, ProfileDelegate{
    
    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNip: UILabel!
    @IBOutlet weak var lblJabatan: UILabel!
    @IBOutlet weak var lblUnitKerja: UILabel!
    @IBOutlet weak var lblGolongan: UILabel!
    @IBOutlet weak var lblSkpd: UILabel!
    @IBOutlet weak var lblNoTelp: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ProfilePresenter(view: self)
        ivProfile.layer.cornerRadius = ivProfile.frame.size.width / 2
        getProfileDetail()
    }
    
    private func getProfileDetail(){
        let paramInput = ParamInput()
        paramInput.userId = DBUtil.getInstance().getCurrentUser()!.userId!
        presenter.getProfileDetail(paramInput: paramInput)
    }
    
    private func initProfile(model: ProfileModel){
        ivProfile.kf.setImage(with: URL(string: model.photo), placeholder: UIImage(named: "ProfilePlaceholder"))
        lblName.text = model.pegawaiNama
        lblNip.text = model.pegawaiNip
        lblJabatan.text = model.jabatanNama
        lblUnitKerja.text = model.unitKerjaInitial
        lblGolongan.text = model.golonganNama
        lblSkpd.text = model.skpdInitial
        lblNoTelp.text = model.pegawaiTelp
    }
    
    func didLoadProfile(model: ProfileModel) {
        initProfile(model: model)
    }
    
    func taskDidBegin() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func taskDidFinish() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func taskDidError(message: String) {
        showError(title: nil, message: message)
    }
}
