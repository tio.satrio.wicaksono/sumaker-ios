//
//  ProfilePresenter.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 12/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class ProfilePresenter: BasePresenter<ProfileDelegate>{
    func getProfileDetail(paramInput: ParamInput){
        view.taskDidBegin()
        Api.requestGetProfileDetail(paramInput: paramInput, completion: { model in
            self.view.didLoadProfile(model: model)
            self.view.taskDidFinish()
        }) { (err) in
            self.view.taskDidFinish()
            self.view.taskDidError(message: err.localizedDescription)
        }
    }
}

protocol ProfileDelegate: BaseDelegate {
    func didLoadProfile(model: ProfileModel)
}
