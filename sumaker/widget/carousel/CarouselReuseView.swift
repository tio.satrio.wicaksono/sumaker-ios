//
//  CarouselView.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 03/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class CarouselReuseView: UIView {
    typealias Action = (CarouselReuseView, Int) -> Void
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var ivCarousel: UIImageView!
    @IBOutlet weak var lblCarousel: UILabel!
    @IBOutlet weak var backgroundBadge: UIView!
    @IBOutlet weak var lblBadge: UILabel!
    var tapped: Action?
    var index: Int!
    override func awakeFromNib() {
        index = 0
        backgroundBadge.layer.cornerRadius = min(backgroundBadge.frame.size.height, backgroundBadge.frame.size.width) / 2.0
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap)))
    }
    
    @objc func onTap(gesture: UITapGestureRecognizer){
        tapped?(self, index)
    }
}
