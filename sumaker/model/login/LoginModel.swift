//
//  LoginModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 11/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct LoginModel: Codable {
    var token: String
    var userId: String
    
    enum CodingKeys: String, CodingKey {
        case token
        case userId = "user_id"
    }
}
