//
//  ProfileModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 12/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct ProfileModel: Codable {
    var pegawaiId: String
    var pegawaiNip: String
    var pegawaiNama: String
    var pegawaiTelp: String
    var jabatanId: String
    var jabatanNama: String
    var unitKerjaId: String
    var unitKerjaNama: String
    var unitKerjaInitial: String
    var golonganId: String
    var golonganNama: String
    var skpdId: String
    var skpdNama: String
    var skpdInitial: String
    var photo: String
    var paraf: String
    var ttd: String
    
    enum CodingKeys: String, CodingKey {
        case pegawaiId = "pegawai_id"
        case pegawaiNip = "pegawai_nip"
        case pegawaiNama = "pegawai_nama"
        case pegawaiTelp = "pegawai_telp"
        case jabatanId = "jabatan_id"
        case jabatanNama = "jabatan_nama"
        case unitKerjaId = "unitkerja_id"
        case unitKerjaNama = "unitkerja_nama"
        case unitKerjaInitial = "unitkerja_initial"
        case golonganId = "golongan_id"
        case golonganNama = "golongan_nama"
        case skpdId = "skpd_id"
        case skpdNama = "skpd_nama"
        case skpdInitial = "skpd_initial"
        case photo
        case paraf
        case ttd
    }
}
