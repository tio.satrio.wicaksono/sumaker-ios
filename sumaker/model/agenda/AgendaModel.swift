//
//  AgendaModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 19/12/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct AgendaModel: Codable {
    var mailId: String
    var awalKegiatan: String
    var akhirKegiatan: String
    var tempatKegiatan: String
    var prihal: String
    
    enum CodingKeys: String, CodingKey{
        case mailId = "mail_id"
        case awalKegiatan = "d_awal_kegiatan"
        case akhirKegiatan = "d_akhir_kegiatan"
        case tempatKegiatan = "tempat_kegiatan"
        case prihal = "prihal"
    }
}
