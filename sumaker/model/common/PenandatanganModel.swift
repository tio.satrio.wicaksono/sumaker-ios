//
//  PenandatanganModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 18/12/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct PenandatanganModel: Codable {
    var id: String
    var namaPegawai: String
    var jabatan: String
    var unitKerja: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case namaPegawai = "nama_pegawai"
        case jabatan = "jabatan"
        case unitKerja = "unitkerja"
    }
}
