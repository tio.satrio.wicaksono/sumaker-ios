//
//  SifatSuratModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 28/08/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct SifatSuratModel: Codable {
    var id: String
    var label: String
    
    enum CodingKeys: String, CodingKey {
        case id = "sifat_id"
        case label = "sifat_label"
    }
}
