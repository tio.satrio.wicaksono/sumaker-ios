//
//  SuratDariModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 24/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class PickerViewModel{
    var id: Int
    var nama: String
    
    init(id: Int, nama: String) {
        self.id = id
        self.nama = nama
    }
}
