//
//  CaraouselMenuModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 30/05/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation
import UIKit

class CarouselMenuModel{
    var ivMenu: UIImage!
    var textMenu: String
    init(image: UIImage, menu: String) {
        ivMenu = image
        textMenu = menu
    }
}
