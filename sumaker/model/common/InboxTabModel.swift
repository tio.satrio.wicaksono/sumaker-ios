//
//  InboxTabModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 13/06/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct InboxTabModel: Codable {
    var id: String
    var label: String
    
    enum CodingKeys: String, CodingKey {
        case id = "origin_id"
        case label = "origin_label"
    }
}
