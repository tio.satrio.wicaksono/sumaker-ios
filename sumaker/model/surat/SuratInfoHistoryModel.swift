//
//  SuratInfoHistoryModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 04/09/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct SuratInfoHistoryModel: Codable {
    var pegawaiDari: String
    var jabatanDari: String
    var unitKerjaDari: String
    var paraf: String
    var pegawaiKe: String
    var jabatanKe: String
    var unitKerjaKe: String
    var tglKirim: String
    var tglBaca: String?
    
    enum CodingKeys: String, CodingKey {
        case pegawaiDari = "pegawai_dari"
        case jabatanDari = "jabatan_dari"
        case unitKerjaDari = "unitkerja_dari"
        case paraf
        case pegawaiKe = "pegawai_ke"
        case jabatanKe = "jabatan_ke"
        case unitKerjaKe = "unitkerja_ke"
        case tglKirim = "tgl_kirim"
        case tglBaca = "tgl_baca"
    }
}
