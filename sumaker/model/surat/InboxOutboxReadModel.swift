//
//  InboxReadModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 16/12/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct InboxOutboxReadModel: Codable {
    var namaPegawaiDari: String
    var fotoPegawaiDari: String
    var namaPegawaiKe: String
    var tglSurat: String
    var suratKepada: String
    var sifatSurat: String
    var perihal: String
    var ringkasanSurat: String
    var catatanDisposisi: String?
    var fileSurat: String?
    
    enum CodingKeys: String, CodingKey {
        case namaPegawaiDari = "nama_pegawai_dari"
        case fotoPegawaiDari = "foto_pegawai_dari"
        case namaPegawaiKe  = "nama_pegawai_ke"
        case tglSurat = "tgl_surat"
        case suratKepada = "surat_kepada"
        case sifatSurat = "sifat_surat"
        case perihal = "perihal"
        case ringkasanSurat = "ringkasan_surat"
        case catatanDisposisi = "catatan_disposisi"
        case fileSurat = "file_surat"
    }
}
