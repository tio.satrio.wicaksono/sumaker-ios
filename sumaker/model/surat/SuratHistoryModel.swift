//
//  SuratHistoryModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 16/07/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class SuratHistoryModel {
    var image: String = ""
    var nama: String = ""
    var kepada: String = ""
    var waktu: String = ""
    var disposisiTindakan: String = ""
    var catatan: String = ""
}
