//
//  SuratModel1.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 31/08/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct SuratKeluarModel: Codable{
    var id: String
    var asalSurat: String
    var namaKonseptor: String
    var bidangKonseptor: String
    var tglDibuat: String
    var namaPenerima: String
    var tglDikirim: String
    var perihal: String
    var idSifatSurat: String
    var sifatSurat: String
    var statusDibaca: String
    var statusDiproses: String? = nil
    var batasLimitWaktu: String? = nil
    
    enum CodingKeys: String, CodingKey {
        case id
        case asalSurat = "asal_surat"
        case namaKonseptor = "nama_konseptor"
        case bidangKonseptor = "bidang_konseptor"
        case tglDibuat = "tgl_dibuat"
        case namaPenerima = "nama_penerima"
        case tglDikirim = "tgl_dikirim"
        case perihal
        case idSifatSurat = "id_sifat_surat"
        case sifatSurat = "label_sifat_surat"
        case statusDibaca = "status_dibaca"
        case statusDiproses = "status_diproses"
        case batasLimitWaktu = "batas_limit_waktu"
    }
}
