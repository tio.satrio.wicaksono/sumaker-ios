//
//  DitujukanKepadaModel1.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 11/09/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct DitujukanKepadaModel1: Codable{
    var tree: [Parent]
}

struct Parent: Codable {
    var id: String
    var text: String
    var parent: String
    var state: String
    var children: [ChildrenDitujukanKepada]
}

struct ChildrenDitujukanKepada: Codable {
    var id: String
    var text: String
    var parent: String
}
