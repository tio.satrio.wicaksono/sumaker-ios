//
//  SuratInfoModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 15/08/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

class SuratInfoModel{
    var tglKirim: String
    var tglBaca: String
    var dari: String
    var tujuan: String
    var parafImage: String
    
    init(tglKirim: String, tglBaca: String, dari: String, tujuan: String, parafImage: String) {
        self.tglKirim = tglKirim
        self.tglBaca = tglBaca
        self.dari = dari
        self.tujuan = tujuan
        self.parafImage = parafImage
    }
}
