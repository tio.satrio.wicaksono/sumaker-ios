//
//  SifatSuratDistribusiModel.swift
//  sumaker
//
//  Created by Satrio Wicaksono on 17/10/2018.
//  Copyright © 2018 Satrio Wicaksono. All rights reserved.
//

import Foundation

struct SuratSifatDistribusiModel: Codable {
    var id: String
    var label: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "sifat"
    }
}

